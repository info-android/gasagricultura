package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.ConstantClass;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MyDbClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ChooseUserActivity extends AppCompatActivity {
    private LinearLayout farmer_layout, product;
    private DrawerLayout drawer_layout;
    private ImageView cart_img;
    private TextView welcom_name_tv, first_tv, second_tv;
    private TextView title_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(ChooseUserActivity.this);
        setContentView(R.layout.activity_choose_user);
        farmer_layout = findViewById(R.id.farmer_layout);
        product = findViewById(R.id.product);
        cart_img = findViewById(R.id.cart_img);
        drawer_layout = findViewById(R.id.drawer_layout);
        welcom_name_tv = findViewById(R.id.welcom_name_tv);
        first_tv = findViewById(R.id.first_tv);
        second_tv = findViewById(R.id.second_tv);
        title_tv = findViewById(R.id.title);


        final boolean is_logged_in = PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).getBoolean("is_logged_in", false);
        MyDbClass myDbClass=new MyDbClass(ChooseUserActivity.this);
        SQLiteDatabase sqLiteDatabase=myDbClass.getWritableDatabase();
        if (is_logged_in) {
            String user_type = PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).getString("user_type", "");
            if (user_type.equals("A")) {
                Intent intent = new Intent(ChooseUserActivity.this, ManageOpenCloseActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return;
            }
            Gson gson = new Gson();
            String sqlQuery="SELECT * FROM " + MyDbClass.DATA_TABLE;
            Cursor cursor= sqLiteDatabase.rawQuery(sqlQuery,null);
            if (cursor.moveToNext()) {
                String cartArrayAsString = cursor.getString(cursor.getColumnIndex(MyDbClass.DATA));
                Type type = new TypeToken<ArrayList<MethodClass.CartClass>>() {
                }.getType();
                if (!cartArrayAsString.equals(null) && !cartArrayAsString.equals("null") && !cartArrayAsString.equals("")) {
                    ConstantClass.CART_ARRAY = gson.fromJson(cartArrayAsString, type);
                }
            }
            cursor.close();

            get_contents();
            farmer_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ChooseUserActivity.this, FarmerListActivity.class);
                    startActivity(intent);
                }
            });
            product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ChooseUserActivity.this, FilterActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            long deletedRows = sqLiteDatabase.delete(MyDbClass.DATA_TABLE, null, null);
            Intent intent = new Intent(ChooseUserActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

         /*Gson gson = new Gson();
            String cartArrayAsString = PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).getString("cartArray", "");
            Type type = new TypeToken<ArrayList<MethodClass.CartClass>>() {}.getType();
            if (!cartArrayAsString.equals(null) && !cartArrayAsString.equals("null") && !cartArrayAsString.equals("")) {
                ConstantClass.CART_ARRAY = gson.fromJson(cartArrayAsString, type);
            }*/
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ChooseUserActivity.this);
        MethodClass.setMenu(ChooseUserActivity.this);
        welcom_name_tv.setText(getResources().getString(R.string.welcome_space)+" "+ PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).getString("user_fname", ""));
    }


    private void get_contents() {
        if (!isNetworkConnected(ChooseUserActivity.this)) {
            MethodClass.network_error_alert(ChooseUserActivity.this);
            return;
        }

        MethodClass.showProgressDialog(ChooseUserActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "shop-content";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ChooseUserActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ChooseUserActivity.this, response);
                    if (resultResponse != null) {

                        JSONObject content = resultResponse.getJSONObject("content");
                        String id = content.getString("id");
                        String title = content.getString("title");
                        String first_txt = content.getString("first_txt");
                        String scnd_txt = content.getString("scnd_txt");

                        first_tv.setText(first_txt);
                        second_tv.setText(scnd_txt);

                        JSONObject shoptime = resultResponse.getJSONObject("shoptime");
                        PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).edit().putString("shoptime", String.valueOf(shoptime)).commit();
                        String from_time = shoptime.getString("from_time");
                        String to_time = shoptime.getString("to_time");
                        Integer from_day = Integer.valueOf(shoptime.getString("from_day"));
                        Integer to_day = Integer.valueOf(shoptime.getString("to_day"));

                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        SimpleDateFormat printdateFormat = new SimpleDateFormat("HH:mm");
                        try {
                            String[] days = {"Days", getResources().getString(R.string.monday),
                                    getResources().getString(R.string.tuesday),
                                    getResources().getString(R.string.wednesday),
                                    getResources().getString(R.string.thursday),
                                    getResources().getString(R.string.friday),
                                    getResources().getString(R.string.saturday),
                                    getResources().getString(R.string.sunday)
                            };

                            Date fromtime = dateFormat.parse(from_time);
                            Date totime = dateFormat.parse(to_time);

                            String settitle = days[from_day] + ", " + printdateFormat.format(fromtime) + " - " + days[to_day] + ", " + printdateFormat.format(totime);
                            title_tv.setText(settitle);



                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ChooseUserActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );
                MethodClass.hideProgressDialog(ChooseUserActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ChooseUserActivity.this);
                } else {
                    MethodClass.error_alert(ChooseUserActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChooseUserActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ChooseUserActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
