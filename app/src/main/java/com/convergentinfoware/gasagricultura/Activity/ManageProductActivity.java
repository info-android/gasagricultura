package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.ManageProductAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.CATEGORY_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.DESCRIPTION;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.IS_AVAILABLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ManageProductActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title, user_name_tv;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    private LinearLayout user_lay;
    private ImageView cart_img;
    private LinearLayout no_data_lay;
    private CircleImageView user_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_manage_product);
        hide_keyboard(this);
        drawer_layout = findViewById(R.id.drawer_layout);
        user_lay = findViewById(R.id.user_lay);
        user_lay.setVisibility(View.GONE);
        recy_view = findViewById(R.id.recy_view);
        title = findViewById(R.id.title);
        user_name_tv = findViewById(R.id.user_name_tv);
        user_image = findViewById(R.id.user_image);
        no_data_lay = findViewById(R.id.no_data_lay);
        title.setText(R.string.manage_product);
        cart_img = findViewById(R.id.cart_img);
        get_list();
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    public void get_list() {
        if (!isNetworkConnected(ManageProductActivity.this)) {
            MethodClass.network_error_alert(ManageProductActivity.this);
            return;
        }
        recy_view.setAdapter(null);
        String user_id = PreferenceManager.getDefaultSharedPreferences(ManageProductActivity.this).getString("user_id", "");
        MethodClass.showProgressDialog(ManageProductActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-list-seller";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ManageProductActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ManageProductActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray farmer = resultResponse.getJSONArray("farmer");
                        if (farmer.length() > 0) {
                            JSONObject farmerObject = farmer.getJSONObject(0);
                            String id = farmerObject.getString("id");
                            String first_name = farmerObject.getString("first_name");
                            String last_name = farmerObject.getString("last_name");
                            String name = first_name + " " + last_name;
                            String profile_image = PROFILE_IMAGE_URL + farmerObject.getString("profile_image");
                            Picasso.get().load(profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(user_image);

                            user_name_tv.setText(name);
                            user_lay.setVisibility(View.VISIBLE);
                            JSONArray productsJsonArray = farmerObject.getJSONArray("user_products");
                            if (productsJsonArray.length() > 0) {
                                map_list = new ArrayList<HashMap<String, String>>();
                                for (int i = 0; i < productsJsonArray.length(); i++) {
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put(ID, productsJsonArray.getJSONObject(i).getString("id"));
                                    map.put(TITLE, productsJsonArray.getJSONObject(i).getString("title"));
                                    map.put(DESCRIPTION, productsJsonArray.getJSONObject(i).getString("description"));
                                    map.put(PRODCT_IMAGE, productsJsonArray.getJSONObject(i).getString("product_image"));
                                    map.put(USER_ID, productsJsonArray.getJSONObject(i).getString("user_id"));
                                    map.put(CATEGORY_ID, productsJsonArray.getJSONObject(i).getString("category_id"));
                                    map.put(PRODUCT_PRICE, productsJsonArray.getJSONObject(i).getString("product_price"));
                                    map.put(IS_AVAILABLE, productsJsonArray.getJSONObject(i).getString("is_available"));
                                    map_list.add(map);
                                }
                            }
                            ManageProductAdapter manageProductAdapter = new ManageProductAdapter(ManageProductActivity.this, map_list);
                            recy_view.setAdapter(manageProductAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ManageProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    ManageProductActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ManageProductActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ManageProductActivity.this);
                    e.printStackTrace();
                    //Log.e("JSONException",e.toString());
                    no_data_lay.setVisibility(View.VISIBLE);
                    recy_view.setVisibility(View.GONE);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );
                no_data_lay.setVisibility(View.VISIBLE);
                recy_view.setVisibility(View.GONE);
                MethodClass.hideProgressDialog(ManageProductActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ManageProductActivity.this);
                } else {
                    MethodClass.error_alert(ManageProductActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ManageProductActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ManageProductActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ManageProductActivity.this);
        MethodClass.setMenu(ManageProductActivity.this);
    }
}
