package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.SellerOrderListAdapter;
import com.convergentinfoware.gasagricultura.Adapter.SellerOrderProductListAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ARCHIVEED;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.FROMDATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NO_OF_ITEMS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_NO;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.STATUS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TOTAL_WEIGHT_TXT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class SellerOrderListActivity extends AppCompatActivity {
    private LinearLayout farmer_layout, product;
    private DrawerLayout drawer_layout;
    private TextView title;
    private RecyclerView recy_view;
    private LinearLayout no_data_lay;
    private ArrayList<HashMap<String, String>> map_list;
    private Button by_order,by_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_seller_order_list);
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        recy_view = findViewById(R.id.recy_view);
        no_data_lay = findViewById(R.id.no_data_lay);
        title = findViewById(R.id.title);
        by_order = findViewById(R.id.by_order);
        by_product = findViewById(R.id.by_product);
        title.setText(R.string.order_list);
        get_list();
        by_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                by_order.setBackground(getDrawable(R.drawable.button_green_background));
                by_product.setBackground(getDrawable(R.drawable.gray_button_background));
                get_list();
            }
        });
        by_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                by_order.setBackground(getDrawable(R.drawable.gray_button_background));
                by_product.setBackground(getDrawable(R.drawable.button_green_background));
                get_list_product();
            }
        });
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(SellerOrderListActivity.this);
        MethodClass.setMenu(SellerOrderListActivity.this);

    }

    public void get_list() {
        if (!isNetworkConnected(SellerOrderListActivity.this)) {
            MethodClass.network_error_alert(SellerOrderListActivity.this);
            return;
        }
        recy_view.setAdapter(null);
        MethodClass.showProgressDialog(SellerOrderListActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-list-seller";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SellerOrderListActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(SellerOrderListActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray orders = resultResponse.getJSONArray("orders");
                        String fromDate=resultResponse.getString("fromDate");
                        if (orders.length() > 0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < orders.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                String id = orders.getJSONObject(i).getString("id");
                                String order_id = orders.getJSONObject(i).getString("order_id");
                                String order_no = orders.getJSONObject(i).getJSONObject("seller_order").getString("order_no");
                                String archived = orders.getJSONObject(i).getJSONObject("seller_order").getString("archived");
                                String total_weight_txt = orders.getJSONObject(i).getString("total_weight_txt");
                                String order_date = orders.getJSONObject(i).getJSONObject("seller_order").getString("order_date");
                                String order_total = orders.getJSONObject(i).getString("total");
                                String status = orders.getJSONObject(i).getJSONObject("seller_order").getString("status");
                                String total_qty = orders.getJSONObject(i).getString("total_qty");
                                String order_user_id = orders.getJSONObject(i).getJSONObject("seller_order").getJSONObject("order_customer").getString("id");
                                String order_user_first_name = orders.getJSONObject(i).getJSONObject("seller_order").getJSONObject("order_customer").getString("first_name");
                                String order_user_last_name = orders.getJSONObject(i).getJSONObject("seller_order").getJSONObject("order_customer").getString("last_name");
                                String order_user_profile_image = orders.getJSONObject(i).getJSONObject("seller_order").getJSONObject("order_customer").getString("profile_image");
                                map.put(ID, order_id);
                                map.put(ORDER_NO, order_no);
                                map.put(ARCHIVEED, archived);
                                map.put(TOTAL_WEIGHT_TXT, total_weight_txt);
                                map.put(ORDER_DATE, order_date);
                                map.put(ORDER_TOTAL, order_total);
                                map.put(STATUS, status);
                                map.put(NO_OF_ITEMS, total_qty);
                                map.put(USER_ID, order_user_id);
                                map.put(USER_NAME, order_user_first_name + " " + order_user_last_name);
                                map.put(USER_IMAGE, order_user_profile_image);
                                map.put(FROMDATE, fromDate);
                                map_list.add(map);
                            }
                            SellerOrderListAdapter sellerOrderListAdapter = new SellerOrderListAdapter(SellerOrderListActivity.this, map_list);
                            recy_view.setAdapter(sellerOrderListAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(SellerOrderListActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    SellerOrderListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    SellerOrderListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(SellerOrderListActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SellerOrderListActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SellerOrderListActivity.this);
                } else {
                    MethodClass.error_alert(SellerOrderListActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SellerOrderListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(SellerOrderListActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void get_list_product() {
        if (!isNetworkConnected(SellerOrderListActivity.this)) {
            MethodClass.network_error_alert(SellerOrderListActivity.this);
            return;
        }
        recy_view.setAdapter(null);
        MethodClass.showProgressDialog(SellerOrderListActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-order-list-seller";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SellerOrderListActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(SellerOrderListActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray orders = resultResponse.getJSONArray("orders");
                        String fromDate=resultResponse.getString("fromDate");
                        if (orders.length() > 0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < orders.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                String id = orders.getJSONObject(i).getString("id");
                                String order_id = orders.getJSONObject(i).getString("order_id");
                                String total_product = orders.getJSONObject(i).getString("total_product");
                                String current_price = orders.getJSONObject(i).getString("current_price");
                                //String user_id = orders.getJSONObject(i).getString("order_id");
                                String price = orders.getJSONObject(i).getString("price");
                                String qty = orders.getJSONObject(i).getString("qty");
                                JSONObject order_details_product = orders.getJSONObject(i).getJSONObject("order_details_product");
                                String title = order_details_product.getString("title");
                                String created_at = order_details_product.getString("created_at");
                                String price_start_at = order_details_product.getString("price_start_at");
                                String product_image = order_details_product.getString("product_image");

                                JSONObject order_details_unit=orders.getJSONObject(i).getJSONObject("order_details_unit");
                                String unit=order_details_unit.getString("unit");
                                JSONObject order_customer=orders.getJSONObject(i).getJSONObject("order_master").getJSONObject("order_customer");
                                String first_name=order_customer.getString("first_name");
                                String last_name=order_customer.getString("last_name");
                                String name=first_name+" "+last_name;
                                String order_date = orders.getJSONObject(i).getJSONObject("order_master").getString("order_date");

                                float finalPrice=Float.parseFloat(current_price)*Float.parseFloat(total_product);
                                map.put(ID, order_id);
                                map.put(TITLE, title);
                                map.put(QTY, qty);
                                map.put(ORDER_DATE, order_date);
                                map.put(ORDER_TOTAL, String.valueOf(finalPrice));
                                map.put(NO_OF_ITEMS, total_product);
                                //map.put(USER_ID, name);
                                map.put(USER_NAME, name);
                                map.put(PRODUCT_UNIT, unit);
                                map.put(FROMDATE, fromDate);
                                map.put(PRODUCT_IMAGE, product_image);
                                map_list.add(map);
                            }
                            SellerOrderProductListAdapter sellerOrderListAdapter = new SellerOrderProductListAdapter(SellerOrderListActivity.this, map_list);
                            recy_view.setAdapter(sellerOrderListAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(SellerOrderListActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    SellerOrderListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    SellerOrderListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(SellerOrderListActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SellerOrderListActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SellerOrderListActivity.this);
                } else {
                    MethodClass.error_alert(SellerOrderListActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SellerOrderListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(SellerOrderListActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
