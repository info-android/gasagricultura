package com.convergentinfoware.gasagricultura.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.CheckAvailableProductAdapter;
import com.convergentinfoware.gasagricultura.Adapter.MyShoppingBagAdapter;
import com.convergentinfoware.gasagricultura.Helper.ConstantClass;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.AVA_QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class MyShoppingBagActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    private TextView total_item_count_tv, total_qty_tv, pay_amount_tv;
    private Button confirm_button;
    private JSONArray finalArray;
    private NestedScrollView scrollView;
    private String setUnitinTextView = "";
    private boolean checkOrderTime=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_my_shopping_bag);
        drawer_layout = findViewById(R.id.drawer_layout);
        recy_view = findViewById(R.id.recy_view);
        title = findViewById(R.id.title);
        //title.setText(R.string.my_shopping_bag);
        getShopTime();
        total_item_count_tv = findViewById(R.id.total_item_count_tv);
        total_qty_tv = findViewById(R.id.total_qty_tv);
        pay_amount_tv = findViewById(R.id.pay_amount_tv);
        confirm_button = findViewById(R.id.confirm_button);
        scrollView = findViewById(R.id.scrollView);
        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
               /* if (checkOrderTime){
                    placeOrder();
                    //Toast.makeText(MyShoppingBagActivity.this, "open", Toast.LENGTH_SHORT).show();
                }else {
                    new SweetAlertDialog(MyShoppingBagActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getResources().getString(R.string.close_ordering))
                            .setContentText(getResources().getString(R.string.today_is_closer_day_of_ordering))
                            .setConfirmText(getResources().getString(R.string.okay))
                          .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    }).show();
                }*/

            }
        });
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void get_list() {
        MyShoppingBagAdapter myShoppingBagAdapter = new MyShoppingBagAdapter(MyShoppingBagActivity.this, ConstantClass.CART_ARRAY);
        recy_view.setAdapter(myShoppingBagAdapter);
        recy_view.setFocusable(false);
    }

    public void finalCartCalculation() {
        MethodClass.cartCalculation(MyShoppingBagActivity.this);
        if (!(ConstantClass.CART_ARRAY.size() > 0)) {
            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(MyShoppingBagActivity.this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    MyShoppingBagActivity.super.onBackPressed();
                }
            });
            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    MyShoppingBagActivity.super.onBackPressed();
                }
            });
            sweetAlertDialog.show();
            return;
        }
        get_list();
        ArrayList<String> unitArray = new ArrayList<String>();
        scrollView.setVisibility(View.VISIBLE);
        finalArray = new JSONArray();
        float totalCartCount = 0;
        float pay_price = 0;
        for (int i = 0; i < ConstantClass.CART_ARRAY.size(); i++) {
            MethodClass.CartClass cartClass = ConstantClass.CART_ARRAY.get(i);
            totalCartCount = totalCartCount + cartClass.product_qty;
            pay_price = pay_price + Float.parseFloat(cartClass.product_price) * cartClass.product_qty;
            for (int j = 0; j < cartClass.product_qty; j++) {
                unitArray.add(cartClass.product_unit);
            }

            try {
                JSONObject object = new JSONObject();
                object.put("product_id", cartClass.product_id);
                object.put("product_img", cartClass.product_img);
                object.put("product_price", cartClass.product_price);
                object.put("product_total_price", cartClass.product_price);
                object.put("product_qty", cartClass.product_qty);
                object.put("product_title", cartClass.product_title);
                object.put("product_total_qty", cartClass.product_total_qty);
                object.put("product_unit_id", cartClass.product_unit_id);
                object.put("product_unit", cartClass.product_unit);
                object.put("product_user_id", cartClass.product_user_id);
                object.put("product_user_img", cartClass.product_user_img);
                object.put("product_user_name", cartClass.product_user_name);
                finalArray.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayList<String> unitSetTextArray = new ArrayList<>();
        ArrayList<String> tempUnitArray = new ArrayList<>();
        for (int j = 0; j < unitArray.size(); j++) {
            String unit = unitArray.get(j);
            if (!tempUnitArray.contains(unit)) {
                tempUnitArray.add(unit);
                int count = Collections.frequency(unitArray, unit);
                unitSetTextArray.add(String.valueOf(count) + " " + unit);
            }
        }
        setUnitinTextView = TextUtils.join(",", unitSetTextArray);
        Log.e("setUnitinTextView", setUnitinTextView);
        total_qty_tv.setText(getResources().getString(R.string.qta) + setUnitinTextView);
        pay_amount_tv.setText(getResources().getString(R.string.payable_amount)+ pay_price + " €");
        total_item_count_tv.setText(getResources().getString(R.string.your_bought) + ConstantClass.CART_ARRAY.size() + " "+getResources().getString(R.string.items));

    }

    private void placeOrder() {
        MethodClass.showProgressDialog(MyShoppingBagActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "add-order";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("products", finalArray);
        params.put("total_weight_txt", setUnitinTextView);
        JSONObject jsonObject = MethodClass.Json_rpc_format_obj(params);
        Log.e("main_param", jsonObject.toString());

        if (!isNetworkConnected(MyShoppingBagActivity.this)) {
            MethodClass.network_error_alert(MyShoppingBagActivity.this);
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(MyShoppingBagActivity.this);
                Log.e("ResponseOrder", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(MyShoppingBagActivity.this, response);

                    if (resultResponse.has("shop_status")) {
                        if (resultResponse.getString("shop_status").equals("SHOP_CLOSED")) {
                            new SweetAlertDialog(MyShoppingBagActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(resultResponse.getString("message"))
                                    .setContentText(resultResponse.getString("meaning"))
                                    .setConfirmText(getResources().getString(R.string.okay))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    }).show();
                            return;
                        }
                    }
                    if (resultResponse.getString("is_av").equals("N")){
                        JSONArray prod_list=resultResponse.getJSONArray("prod_list");
                        checkProAval(prod_list);
                    }else {
                        ConstantClass.CART_ARRAY.clear();
                        new SweetAlertDialog(MyShoppingBagActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(resultResponse.getString("message"))
                                .setContentText(resultResponse.getString("meaning"))
                                .setConfirmText(getResources().getString(R.string.done))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent intent = new Intent(MyShoppingBagActivity.this, CloseOrderActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).show();
                    }
                } catch (Exception e) {
                    Log.e("Exception",e.toString());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(MyShoppingBagActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MyShoppingBagActivity.this);
                } else {
                    MethodClass.error_alert(MyShoppingBagActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MyShoppingBagActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(MyShoppingBagActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        finalCartCalculation();
        MethodClass.setMenu(MyShoppingBagActivity.this);
    }

    private void getShopTime() {
        if (!isNetworkConnected(MyShoppingBagActivity.this)) {
            MethodClass.network_error_alert(MyShoppingBagActivity.this);
            return;
        }
        MethodClass.showProgressDialog(MyShoppingBagActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "shop-content";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(MyShoppingBagActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(MyShoppingBagActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject shoptime = resultResponse.getJSONObject("shoptime");
                        String force_open = shoptime.getString("force_open");
                        String force_close = shoptime.getString("force_close");
                        String regular_schedule = shoptime.getString("regular_schedule");

                        String from_time = shoptime.getString("from_time");
                        String to_time = shoptime.getString("to_time");
                        String dispach_time = shoptime.getString("dispach_time");
                        Integer dispach_day = Integer.valueOf(shoptime.getString("dispach_day"));
                        Integer from_day = Integer.valueOf(shoptime.getString("from_day"));
                        Integer to_day = Integer.valueOf(shoptime.getString("to_day"));

                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        SimpleDateFormat printdateFormat = new SimpleDateFormat("HH:mm");

                        String[] days = {"Days", getResources().getString(R.string.monday),
                                getResources().getString(R.string.tuesday),
                                getResources().getString(R.string.wednesday),
                                getResources().getString(R.string.thursday),
                                getResources().getString(R.string.friday),
                                getResources().getString(R.string.saturday),
                                getResources().getString(R.string.sunday)
                        };
                        Date dispachtime = dateFormat.parse(dispach_time);
                        Date fromTime = dateFormat.parse(from_time);
                        Date toTime = dateFormat.parse(to_time);
                        String settitle = getResources().getString(R.string.dispatch)+" "+ days[dispach_day] + " " + printdateFormat.format(dispachtime);
                        title.setText(settitle);

                       /* if (resultResponse.getString("shop_status").equals("SHOP_CLOSED")){
                            checkOrderTime = false;
                        }else {
                            checkOrderTime = true;
                        }*/
                       /* if (force_open.equals("Y")){
                            checkOrderTime = true;
                        }else if (regular_schedule.equals("Y")) {
                            checkOrderTime = true;
                        }else if (force_close.equals("Y")){
                            checkOrderTime = false;
                        }*/
                    }
                } catch (Exception e) {
                    MethodClass.hideProgressDialog(MyShoppingBagActivity.this);
                    MethodClass.error_alert(MyShoppingBagActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );
                MethodClass.hideProgressDialog(MyShoppingBagActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MyShoppingBagActivity.this);
                } else {
                    MethodClass.error_alert(MyShoppingBagActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MyShoppingBagActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(MyShoppingBagActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void shoptime() {
        String shoptime_str = PreferenceManager.getDefaultSharedPreferences(MyShoppingBagActivity.this).getString("shoptime", "");
        try {
            JSONObject shoptime = new JSONObject(shoptime_str);

            String force_open = shoptime.getString("force_open");
            String force_close = shoptime.getString("force_close");
            String regular_schedule = shoptime.getString("regular_schedule");

            String from_time = shoptime.getString("from_time");
            String to_time = shoptime.getString("to_time");
            String dispach_time = shoptime.getString("dispach_time");
            Integer dispach_day = Integer.valueOf(shoptime.getString("dispach_day"));
            Integer from_day = Integer.valueOf(shoptime.getString("from_day"));
            Integer to_day = Integer.valueOf(shoptime.getString("to_day"));
            if (to_day<from_day){
                to_day=to_day+7;
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat printdateFormat = new SimpleDateFormat("HH:mm");

            String[] days = {"Days", getResources().getString(R.string.monday),
                    getResources().getString(R.string.tuesday),
                    getResources().getString(R.string.wednesday),
                    getResources().getString(R.string.thursday),
                    getResources().getString(R.string.friday),
                    getResources().getString(R.string.saturday),
                    getResources().getString(R.string.sunday)
            };
            Date dispachtime = dateFormat.parse(dispach_time);
            Date fromTime = dateFormat.parse(from_time);
            Date toTime = dateFormat.parse(to_time);
            String settitle = getResources().getString(R.string.dispatch)+" "+ days[dispach_day] + " " + printdateFormat.format(dispachtime);
            title.setText(settitle);

            if (force_open.equals("Y")){
                checkOrderTime = true;
            }else if (regular_schedule.equals("Y")) {
                checkOrderTime = true;
            }else if (force_close.equals("Y")){
                checkOrderTime = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void checkProAval(JSONArray jsonArray){
       try {
           ArrayList<HashMap<String,String>> mapArrayList=new ArrayList<>();
           final Dialog checkAvaDialog = new Dialog(MyShoppingBagActivity.this);
           checkAvaDialog.getWindow().setBackgroundDrawableResource(R.color.transpaent);
           checkAvaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
           checkAvaDialog.setContentView(R.layout.avilable_product_lay);
           checkAvaDialog.setCanceledOnTouchOutside(false);
           RecyclerView recy_view=(RecyclerView) checkAvaDialog.findViewById(R.id.recy_view);
           ImageView cross= (ImageView) checkAvaDialog.findViewById(R.id.cross);
           cross.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   checkAvaDialog.dismiss();
               }
           });

           for (int i = 0; i < jsonArray.length(); i++) {
               String product_id = jsonArray.getJSONObject(i).getString("product_id");
               String product_img = jsonArray.getJSONObject(i).getString("product_img");
               String product_price = jsonArray.getJSONObject(i).getString("product_price");
               String product_total_price = jsonArray.getJSONObject(i).getString("product_total_price");
               String product_qty = jsonArray.getJSONObject(i).getString("product_qty");
               String product_title = jsonArray.getJSONObject(i).getString("product_title");
               String product_total_qty = jsonArray.getJSONObject(i).getString("product_total_qty");
               String product_unit_id = jsonArray.getJSONObject(i).getString("product_unit_id");
               String product_unit = jsonArray.getJSONObject(i).getString("product_unit");
               String product_user_id = jsonArray.getJSONObject(i).getString("product_user_id");
               String product_user_img = jsonArray.getJSONObject(i).getString("product_user_img");
               String product_user_name = jsonArray.getJSONObject(i).getString("product_user_name");
               String prod_av = jsonArray.getJSONObject(i).getString("prod_av");
               HashMap<String,String> hashMap =new HashMap<String, String>();
               hashMap.put(PRODUCT_ID,product_id);
               hashMap.put(PRODUCT_IMAGE,product_img);
               hashMap.put(TITLE,product_title);
               hashMap.put(AVA_QTY,prod_av);
               hashMap.put(PRODUCT_UNIT,product_unit);
               Float price= Float.parseFloat(prod_av) * Float.parseFloat(product_price);
               hashMap.put(PRODUCT_PRICE, String.valueOf(price));
               hashMap.put(PROFILE_IMAGE, product_user_img);
               hashMap.put(USER_NAME, product_user_name);
               mapArrayList.add(hashMap);
           }
           CheckAvailableProductAdapter checkAvailableProductAdapter=new CheckAvailableProductAdapter(MyShoppingBagActivity.this,mapArrayList);
           recy_view.setAdapter(checkAvailableProductAdapter);
           recy_view.setFocusable(false);
           checkAvaDialog.show();
       }catch (Exception e){
           Log.e("error",e.toString());
       }

    }
}






 /* if (cartClass.product_unit.equals("kg")) {
                total_qty_kg = total_qty_kg + 1;
            } else {
                total_qty_pz = total_qty_pz + 1;
            }*/


/*
    public void finalCartCalculation() {
        MethodClass.cartCalculation(MyShoppingBagActivity.this);
        if (!(ConstantClass.CART_ARRAY.size() > 0)) {
            new SweetAlertDialog(MyShoppingBagActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("No Data").setContentText("No Data Available").setConfirmText("Ok").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    MyShoppingBagActivity.super.onBackPressed();
                }
            }).show();
            return;
        }
        get_list();
        scrollView.setVisibility(View.VISIBLE);
        finalArray = new JSONArray();
        int totalCartCount = 0;
        float pay_price = 0;
        int total_qty_kg = 0;
        int total_qty_pz = 0;
        for (int i = 0; i < ConstantClass.CART_ARRAY.size(); i++) {
            MethodClass.CartClass cartClass = ConstantClass.CART_ARRAY.get(i);
            totalCartCount = totalCartCount + cartClass.product_qty;
            pay_price = pay_price + Float.parseFloat(cartClass.product_price) * cartClass.product_qty;
            if (cartClass.product_unit.equals("kg")) {
                total_qty_kg = total_qty_kg + 1;
            } else {
                total_qty_pz = total_qty_pz + 1;
            }

            try {
                JSONObject object=new JSONObject();
                object.put("product_id",cartClass.product_id);
                object.put("product_img",cartClass.product_img);
                object.put("product_price",cartClass.product_price);
                object.put("product_qty",cartClass.product_qty);
                object.put("product_title",cartClass.product_title);
                object.put("product_total_qty",cartClass.product_total_qty);
                object.put("product_unit_id",cartClass.product_unit_id);
                object.put("product_unit",cartClass.product_unit);
                object.put("product_user_id",cartClass.product_user_id);
                object.put("product_user_img",cartClass.product_user_img);
                object.put("product_user_name",cartClass.product_user_name);
                finalArray.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        total_item_count_tv.setText("Your bought " + String.valueOf(totalCartCount) + " items");
        pay_amount_tv.setText("payable amount: " + pay_price + " €");
        if (total_qty_kg > 0 && total_qty_pz > 0) {
            total_qty_tv.setText("disp: " + total_qty_kg + " kg, " + total_qty_pz + " pz");
        } else if (total_qty_kg > 0) {
            total_qty_tv.setText("disp: " + total_qty_kg + " kg");
        } else if (total_qty_pz > 0) {
            total_qty_tv.setText("disp: " + total_qty_pz + " pz");
        }

    }*/
