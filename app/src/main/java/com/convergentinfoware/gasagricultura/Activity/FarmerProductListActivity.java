package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.FarmerProductListAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.CATEGORY_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.DESCRIPTION;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class FarmerProductListActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title,user_name_tv;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    String user_id="";
    private LinearLayout full_layout,farmer_layout;
    private LinearLayout no_data_lay;
    private CircleImageView user_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_farmer_product_list);
        drawer_layout=findViewById(R.id.drawer_layout);
        farmer_layout=findViewById(R.id.farmer_layout);
        full_layout=findViewById(R.id.full_layout);
        full_layout.setVisibility(View.GONE);
        recy_view=findViewById(R.id.recy_view);
        recy_view.setFocusable(false);
        title=findViewById(R.id.title);
        user_name_tv=findViewById(R.id.user_name_tv);
        user_image=findViewById(R.id.user_image);
        no_data_lay=findViewById(R.id.no_data_lay);
        title.setText(R.string.farmer);
        user_id=getIntent().getStringExtra("user_id");
        get_list(user_id);
        farmer_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(FarmerProductListActivity.this,FarmerProductProfileActivity.class);
                intent.putExtra("user_id",user_id);
                startActivity(intent);
            }
        });
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    } public void back(View view) {
        super.onBackPressed();
    }

   

    public void get_list(String user_id) {
        if (!isNetworkConnected(FarmerProductListActivity.this)){
            MethodClass.network_error_alert(FarmerProductListActivity.this);
            return;
        }
        MethodClass.showProgressDialog(FarmerProductListActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-list-user";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id",user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(FarmerProductListActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(FarmerProductListActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray farmer = resultResponse.getJSONArray("farmer");
                        JSONObject farmerObject = farmer.getJSONObject(0);
                        String id=farmerObject.getString("id");
                        String first_name = farmerObject.getString("first_name");
                        String last_name = farmerObject.getString("last_name");
                        String user_img = farmerObject.getString("profile_image");
                        String name = first_name + " " + last_name;
                        user_name_tv.setText(name);
                        String profile_image = PROFILE_IMAGE_URL + user_img;
                        Picasso.get().load(profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(user_image);

                        JSONArray productsJsonArray = farmerObject.getJSONArray("user_products_available");
                        if (productsJsonArray.length()>0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < productsJsonArray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(USER_ID, id);
                                map.put(USER_NAME, name);
                                map.put(USER_IMAGE, user_img);
                                map.put(ID, productsJsonArray.getJSONObject(i).getString("id"));
                                map.put(TITLE, productsJsonArray.getJSONObject(i).getString("title"));
                                map.put(DESCRIPTION, productsJsonArray.getJSONObject(i).getString("description"));
                                map.put(PRODCT_IMAGE, productsJsonArray.getJSONObject(i).getString("product_image"));
                                map.put(USER_ID, productsJsonArray.getJSONObject(i).getString("user_id"));
                                map.put(CATEGORY_ID, productsJsonArray.getJSONObject(i).getString("category_id"));
                                map.put(PRODUCT_PRICE, productsJsonArray.getJSONObject(i).getString("product_price"));
                                map_list.add(map);
                            }
                            FarmerProductListAdapter farmerProductListAdapter =new FarmerProductListAdapter(FarmerProductListActivity.this,map_list);
                            recy_view.setAdapter(farmerProductListAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        }else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(FarmerProductListActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    FarmerProductListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    FarmerProductListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                        full_layout.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(FarmerProductListActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException",e.toString());
                    full_layout.setVisibility(View.GONE);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error2", error.getMessage() );
                full_layout.setVisibility(View.GONE);
                MethodClass.hideProgressDialog(FarmerProductListActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(FarmerProductListActivity.this);
                } else {
                    MethodClass.error_alert(FarmerProductListActivity.this);
                }
            }
        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FarmerProductListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(FarmerProductListActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(FarmerProductListActivity.this);
        MethodClass.setMenu(FarmerProductListActivity.this);
    }

}
