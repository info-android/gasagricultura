package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.FarmerAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class FarmerListActivity extends AppCompatActivity {
    private ArrayList<HashMap<String, String>> map_list;
    private RecyclerView recy_view;
    private DrawerLayout drawer_layout;
    private LinearLayout no_data_lay;
    private EditText type_et;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_farmer_list);
        hide_keyboard(this);
        title=findViewById(R.id.title);
        title.setText(R.string.farmers);
        recy_view=findViewById(R.id.recy_view);
        drawer_layout=findViewById(R.id.drawer_layout);
        no_data_lay=findViewById(R.id.no_data_lay);
        type_et = findViewById(R.id.type_et);


        type_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    get_list();
                    return true;
                }
                return false;
            }
        });


        get_list();
    }

    private void get_list() {
        if (!isNetworkConnected(FarmerListActivity.this)){
            MethodClass.network_error_alert(FarmerListActivity.this);
            return;
        }

        String user_id=PreferenceManager.getDefaultSharedPreferences(FarmerListActivity.this).getString("user_id","");
        MethodClass.showProgressDialog(FarmerListActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "list-farmer";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id","");
        params.put("keyword",type_et.getText().toString());
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("jsonObject",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(FarmerListActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(FarmerListActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray jsonArray = resultResponse.getJSONArray("farmer_list");
                        if (jsonArray.length()>0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(ID, jsonArray.getJSONObject(i).getString("id"));
                                String first_name = jsonArray.getJSONObject(i).getString("first_name");
                                String last_name = jsonArray.getJSONObject(i).getString("last_name");
                                String profile_image = jsonArray.getJSONObject(i).getString("profile_image");
                                String name = first_name + " " + last_name;
                                map.put(NAME, name);
                                map.put(PROFILE_IMAGE, profile_image);
                                map_list.add(map);
                            }
                            FarmerAdapter farmerAdapter=new FarmerAdapter(FarmerListActivity.this,map_list);
                            recy_view.setAdapter(farmerAdapter);
                            recy_view.setFocusable(false);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        }else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(FarmerListActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    FarmerListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    FarmerListActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(FarmerListActivity.this);
                    e.printStackTrace();
                    no_data_lay.setVisibility(View.VISIBLE);
                    recy_view.setVisibility(View.GONE);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );
                no_data_lay.setVisibility(View.VISIBLE);
                recy_view.setVisibility(View.GONE);
                MethodClass.hideProgressDialog(FarmerListActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(FarmerListActivity.this);
                } else {
                    MethodClass.error_alert(FarmerListActivity.this);
                }
            }
        }){
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FarmerListActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(FarmerListActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    } public void back(View view) {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(FarmerListActivity.this);
        MethodClass.setMenu(FarmerListActivity.this);
    }
}

