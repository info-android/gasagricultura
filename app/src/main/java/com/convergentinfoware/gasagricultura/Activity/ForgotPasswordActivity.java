package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;


public class ForgotPasswordActivity extends AppCompatActivity {
    private Button sbmt_btn;
    private EditText edt_email;
    private String get_email="";
    private TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_forgot_password);
        MethodClass.hide_keyboard(ForgotPasswordActivity.this);
        sbmt_btn=findViewById(R.id.sbmt_btn);
        edt_email=findViewById(R.id.email_et);
        title=findViewById(R.id.title);
        title.setText(getResources().getString(R.string.forgot_password_only));

        sbmt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword();
            }
        });

    }
    public void back(View view) {
        onBackPressed();
        finish();
    }



    private void forgotPassword() {
        get_email = edt_email.getText().toString().trim();

        if (get_email.length() == 0) {
            edt_email.setError(getResources().getString(R.string.please_enter_email_address));
            edt_email.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(get_email)) {
            edt_email.setError(getResources().getString(R.string.invalid_email_address));
            edt_email.requestFocus();
            return;
        }
        if (!isNetworkConnected(ForgotPasswordActivity.this)){
            MethodClass.network_error_alert(ForgotPasswordActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ForgotPasswordActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "forgot-pass";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email);
        JSONObject jsonObject=MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ForgotPasswordActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ForgotPasswordActivity.this,response);
                    if (jsonObject!=null) {
                        String vcode = jsonObject.getString("vcode");
                        Intent intent = new Intent(ForgotPasswordActivity.this, VerifyActivity.class);
                        intent.putExtra("email", get_email);
                        intent.putExtra("code", vcode);
                        intent.putExtra("type", "F");
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ForgotPasswordActivity.this);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ForgotPasswordActivity.this);
                if (error.toString().contains("ConnectException")){
                    MethodClass.network_error_alert(ForgotPasswordActivity.this);
                }
                else {
                    MethodClass.error_alert(ForgotPasswordActivity.this);
                }
            }
        });

        MySingleton.getInstance(ForgotPasswordActivity.this).addToRequestQueue(jsonObjectRequest);

    }

}
