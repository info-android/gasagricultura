package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.ConstantClass;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ProductDetailsActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private TextView product_name_tv, product_price_tv, product_avi_qty_tv, product_desc_tv, productCategory_tv,farmer_name_tv;
    private ImageView product_image;
    private LinearLayout choose_product_img_lay;
    private Button add_to_cart_btn;
    private ScrollView scrollView;
    private CircleImageView user_image;
    private LinearLayout seller_lay;


    private ArrayList<MethodClass.StringWithTag> catArrayList, unitArrayList;
    private String product_cat_id = "0", product_unit_id = "0";
    private String product_id = "";
    private ArrayAdapter catArrayAdapter, unitArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_poduct_details);
        hide_keyboard(this);
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        title.setText(R.string.product_details);
        product_name_tv = findViewById(R.id.product_name_tv);
        product_price_tv = findViewById(R.id.product_price_tv);
        product_desc_tv = findViewById(R.id.product_desc_tv);
        product_avi_qty_tv = findViewById(R.id.product_avi_qty_tv);
        productCategory_tv = findViewById(R.id.cat_tv);
        farmer_name_tv = findViewById(R.id.farmer_name_tv);
        product_image = findViewById(R.id.product_image);
        user_image = findViewById(R.id.user_image);
        choose_product_img_lay = findViewById(R.id.choose_product_img_lay);
        scrollView = findViewById(R.id.scrollView);
        seller_lay = findViewById(R.id.seller_lay);
        scrollView.setVisibility(View.GONE);
        add_to_cart_btn = findViewById(R.id.add_to_cart_btn);
        product_id = getIntent().getStringExtra("product_id");
        productDetails();

    }



    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ProductDetailsActivity.this);
        MethodClass.setMenu(ProductDetailsActivity.this);
    }


    private void productDetails() {
        if (!isNetworkConnected(ProductDetailsActivity.this)){
            MethodClass.network_error_alert(ProductDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject product_dataOBJ = resultResponse.getJSONObject("product_data");
                        final String product_id=product_dataOBJ.getString("id");
                        final String product_title=product_dataOBJ.getString("title");
                        product_name_tv.setText(product_title);
                        product_desc_tv.setText(product_dataOBJ.getString("description"));
                        productCategory_tv.setText(product_dataOBJ.getJSONObject("product_category").getString("title"));


                        final String product_price=product_dataOBJ.getJSONArray("product_price").getJSONObject(0).getString("price");
                        final String product_unit=product_dataOBJ.getJSONArray("product_price").getJSONObject(0).getJSONObject("product_price_unit").getString("unit");
                        final String product_unit_id=product_dataOBJ.getJSONArray("product_price").getJSONObject(0).getJSONObject("product_price_unit").getString("id");
                        product_price_tv.setText(product_price.replace(".",",")+ " €/" +product_unit);
                        final String product_qty=product_dataOBJ.getJSONArray("product_price").getJSONObject(0).getString("qty");
                        product_avi_qty_tv.setText(product_qty+" "+product_unit);

                        final String product_ima =  product_dataOBJ.getString("product_image");
                        Log.e("product_image", product_ima);
                        Picasso.get().load(PRODUCT_IMAGE_URL  + product_ima).placeholder(R.drawable.ic_defoult_image).error(R.drawable.ic_defoult_image).into(product_image);

                        JSONObject product_users=product_dataOBJ.getJSONObject("product_users");
                        final String user_id=product_users.getString("id");
                        String sellor_fname=product_users.getString("first_name");
                        String sellor_lname=product_users.getString("last_name");
                        final String seller_name=sellor_fname+" "+sellor_lname;
                        final String profile_image=product_users.getString("profile_image");
                        farmer_name_tv.setText(seller_name);
                        Picasso.get().load(PROFILE_IMAGE_URL+profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(user_image);

                        seller_lay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(ProductDetailsActivity.this,FarmerProductProfileActivity.class);
                                intent.putExtra("user_id",user_id);
                                startActivity(intent);
                                finish();
                            }
                        });

                        add_to_cart_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int arraySize= ConstantClass.CART_ARRAY.size();
                                String userloged_id = PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("user_id", "");
                                if (!user_id.equals(userloged_id)) {
                                float tempQty=0;
                                for (int i = 0; i < ConstantClass.CART_ARRAY.size(); i++) {
                                    MethodClass.CartClass cartClass = ConstantClass.CART_ARRAY.get(i);
                                    if (product_id.equals(cartClass.product_id)) {
                                        tempQty = cartClass.product_qty;
                                        if (Float.valueOf(product_qty) > tempQty){
                                            ConstantClass.CART_ARRAY.remove(i);
                                        }
                                    }
                                }
                                if (Float.valueOf(product_qty) > tempQty){
                                    ConstantClass.CART_ARRAY.add(new MethodClass.CartClass(user_id, seller_name,profile_image, product_id, product_title, product_ima, tempQty+1,Float.valueOf(product_qty), product_price,product_unit_id, String.valueOf(product_unit)));
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),  getResources().getString(R.string.just_added), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }else {
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.product_disp_not_available), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                                MethodClass.cartCalculation(ProductDetailsActivity.this);
                                } else {
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.you_cant_add_own_product), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }

                            }
                        });

                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }




}
