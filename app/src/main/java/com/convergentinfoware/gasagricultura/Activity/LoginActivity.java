package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class LoginActivity extends AppCompatActivity {

    private Button login_btn;
    private TextView forgot_passeord_tv;
    private EditText password_et, email_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_login);

        hide_keyboard(this);
        login_btn = findViewById(R.id.login_btn);
        forgot_passeord_tv = findViewById(R.id.forgot_passeord_tv);
        password_et = findViewById(R.id.password_et);
        email_et = findViewById(R.id.email_et);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        forgot_passeord_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }


    private void login() {
        String get_email = "", get_pswrd = "";
        get_email = email_et.getText().toString().trim();
        get_pswrd = password_et.getText().toString().trim();

        if (get_email.length() == 0) {
            email_et.setError(getResources().getString(R.string.please_enter_email_address));
            email_et.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(get_email)) {
            email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
            email_et.requestFocus();
            return;
        }
        if (get_pswrd.length() == 0) {
            password_et.setError(getResources().getString(R.string.please_enter_password));
            password_et.requestFocus();
            return;
        }

        if (!isNetworkConnected(LoginActivity.this)){
            MethodClass.network_error_alert(LoginActivity.this);
            return;
        }


        MethodClass.showProgressDialog(LoginActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "login";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email);
        params.put("password", get_pswrd);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        final String finalGet_email = get_email;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(LoginActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(LoginActivity.this, response);
                    if (resultResponce != null) {
                        String status = resultResponce.getString("status");
                        // if (status.equals("A") || status.equals("R"))
                        if (status.equals("N")) {
                            Intent intent = new Intent(LoginActivity.this, VerifyActivity.class);
                            intent.putExtra("email", finalGet_email);
                            intent.putExtra("code", "");
                            intent.putExtra("type", "S");
                            startActivity(intent);
                        } else if (status.equals("I")) {
                            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(resultResponce.getJSONObject("message").getString("message"))
                                    .setContentText(resultResponce.getJSONObject("message").getString("meaning"))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            }).show();
                        } else if (status.equals("A")) {
                            String token = resultResponce.getString("token");
                            JSONObject userdata = resultResponce.getJSONObject("userdata");
                            String id = userdata.getString("id");
                            String first_name = userdata.getString("first_name");
                            String last_name = userdata.getString("last_name");
                            String name = first_name + " " + last_name;
                            String email = userdata.getString("email");
                            String mobile = userdata.getString("mobile");
                            String is_seller = userdata.getString("is_seller");
                            String user_type = userdata.getString("user_type");
                            String about = userdata.getString("about");
                            String created_at = userdata.getString("created_at");
                            String updated_at = userdata.getString("updated_at");
                            String profile_image = userdata.getString("profile_image");

                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("token", token).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_fname", first_name).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_lname", last_name).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_name", name).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_id", id).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("email", email).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putBoolean("is_logged_in", true).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("profile_pic", profile_image).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("mobile", mobile).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("is_seller", is_seller).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_type", user_type).commit();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("about", about).commit();
                            if (user_type.equals("A")) {
                                Intent intent = new Intent(LoginActivity.this, ManageOpenCloseActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(LoginActivity.this, ChooseUserActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(LoginActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce",e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(LoginActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(LoginActivity.this);
                } else {
                    MethodClass.error_alert(LoginActivity.this);
                }
            }
        });
        MySingleton.getInstance(LoginActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void signup(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }
}
