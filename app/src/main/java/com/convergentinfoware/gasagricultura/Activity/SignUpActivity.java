package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.error_alert;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.showProgressDialog;

public class SignUpActivity extends AppCompatActivity {


    private EditText first_name_et, last_name_et, email_et, mobile_et, password_et, conf_password_et;
    private Button sign_up_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_sign_up);
        hide_keyboard(this);
        first_name_et = findViewById(R.id.first_name_et);
        last_name_et = findViewById(R.id.last_name_et);
        email_et = findViewById(R.id.email_et);
        mobile_et = findViewById(R.id.mobile_et);
        password_et = findViewById(R.id.password_et);
        conf_password_et = findViewById(R.id.conf_password_et);
        sign_up_btn = findViewById(R.id.sign_up_btn);

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

    }


    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void register() {
        //checking phone  null values to pass server
        String first_name = "", last_name = "", email = "", mobile = "", password = "", conf_password = "";
        first_name = first_name_et.getText().toString().trim();
        last_name = last_name_et.getText().toString().trim();
        email = email_et.getText().toString().trim();
        mobile = mobile_et.getText().toString().trim();
        password = password_et.getText().toString().trim();
        conf_password = conf_password_et.getText().toString().trim();

        if (first_name.length() == 0) {
            first_name_et.setError(getResources().getString(R.string.please_enter_your_first_name));
            first_name_et.requestFocus();
            return;
        }
        if (last_name.length() == 0) {
            last_name_et.setError(getResources().getString(R.string.please_enter_your_last_name));
            last_name_et.requestFocus();
            return;
        }
        if (email.length() == 0) {
            email_et.setError(getResources().getString(R.string.please_enter_your_email_address));
            email_et.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(email)) {
            email_et.setError(getResources().getString(R.string.invalid_email_address));
            email_et.requestFocus();
            return;
        }
        if (mobile.length() < 10) {
            mobile_et.setError(getResources().getString(R.string.please_enter_valid_number));
            mobile_et.requestFocus();
            return;
        }
        if (password.length() == 0) {
            password_et.setError(getResources().getString(R.string.please_enter_password));
            password_et.requestFocus();
            return;
        }
        if (password.length() < 6) {
            password_et.setError(getResources().getString(R.string.please_enter_minimum_six_password));
            password_et.requestFocus();
            return;
        }
        if (conf_password.length() == 0) {
            conf_password_et.setError(getResources().getString(R.string.please_enter_your_confirm_password));
            conf_password_et.requestFocus();
            return;
        }
        if (!password.equals(conf_password)) {
            conf_password_et.setError(getResources().getString(R.string.password_is_not_matched_with_confirm_password));
            conf_password_et.requestFocus();
            return;
        }

        if (!isNetworkConnected(SignUpActivity.this)){
            MethodClass.network_error_alert(SignUpActivity.this);
            return;
        }

        showProgressDialog(SignUpActivity.this);
        String url = getString(R.string.SERVER_URL) + "register";
        Log.e("url", url);
        String authkey = getString(R.string.AUTH_KEY);
        HashMap<String, String> hashMap = new HashMap<>();//create mapping model class to send data of server
        hashMap.put("secret_key", authkey);
        hashMap.put("fname", first_name);
        hashMap.put("lname", last_name);
        hashMap.put("email", email);
        hashMap.put("mobile", mobile);
        hashMap.put("password", password);
        Log.e("register",MethodClass.Json_rpc_format(hashMap).toString());
        final String finalEmail = email;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, MethodClass.Json_rpc_format(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response", response.toString());
                MethodClass.hideProgressDialog(SignUpActivity.this);
                JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this,response);
                try {
                    if (resultResponce !=null){
                        Log.e("resultResponce",resultResponce.toString());
                        String code = resultResponce.getString("otp");
                        Intent intent = new Intent(SignUpActivity.this, VerifyActivity.class);
                        intent.putExtra("email", finalEmail);
                        intent.putExtra("code", code);
                        intent.putExtra("type", "S");
                        startActivity(intent);
                    }
                }catch (Exception e){
                    Log.e("ExceptionElse",e.toString());
                    error_alert(SignUpActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse: ", error.toString());
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException") || error.toString().contains("NoConnectionError")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    Log.e("ErrorElse"," Else");
                    error_alert(SignUpActivity.this);
                }
            }
        });
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}





