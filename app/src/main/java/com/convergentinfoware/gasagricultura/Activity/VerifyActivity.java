package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class VerifyActivity extends AppCompatActivity {
    private Button verify;
    private TextView otp;
    private EditText editText1, editText2, editText3, editText4, editText5, editText6;
    private String edtxt1, edtxt2, edtxt3, edtxt4, edtxt5, edtxt6;
    private TextView resend_tv;
    private String get_email_from_intent = "", code = "", type = "";
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_verify);
        hide_keyboard(this);
        otp = findViewById(R.id.otp);
        verify = findViewById(R.id.verify);
        title = findViewById(R.id.title);
        title.setText(getResources().getString(R.string.verify));

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        editText6 = (EditText) findViewById(R.id.editText6);
        resend_tv = (TextView) findViewById(R.id.resend_tv);

        get_email_from_intent = getIntent().getStringExtra("email");
        type = getIntent().getStringExtra("type");
        code = getIntent().getStringExtra("code");
        otp.setText("your otp=" + code);

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText2.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
        });
        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText3.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText4.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText5.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText6.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText6.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    if (!editText1.getText().toString().equals("") && !editText2.getText().toString().equals("") && !editText3.getText().toString().equals("") && !editText4.getText().toString().equals("")) {
                        Log.e("editText1", editText1.getText().toString());
                        Log.e("editText2", editText2.getText().toString());
                        Log.e("editText3", editText3.getText().toString());
                        Log.e("editText4", editText4.getText().toString());
                        Log.e("editText5", editText5.getText().toString());
                        Log.e("editText6", editText6.getText().toString());
                        verification();
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_enter_all_the_four_digits_in_order_to_continue), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else {
                    //editText5.setBackground(getDrawable(R.drawable.verify_grey_edit_back));
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verification();
            }
        });
        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });
    }


    private void verification() {
        if (editText1.getText().toString().trim().length() == 0 || editText2.getText().toString().trim().length() == 0 || editText3.getText().toString().trim().length() == 0 || editText4.getText().toString().trim().length() == 0) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.entered_verification_code_format_is_invalid), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        edtxt1 = editText1.getText().toString();
        edtxt2 = editText2.getText().toString();
        edtxt3 = editText3.getText().toString();
        edtxt4 = editText4.getText().toString();
        edtxt5 = editText5.getText().toString();
        edtxt6 = editText6.getText().toString();
        String vcode = (edtxt1 + "" + edtxt2 + "" + edtxt3 + "" + edtxt4 + "" + edtxt5 + "" + edtxt6);

        if (!isNetworkConnected(VerifyActivity.this)) {
            MethodClass.network_error_alert(VerifyActivity.this);
            return;
        }

        MethodClass.showProgressDialog(VerifyActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "emailverify";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email_from_intent);
        params.put("vcode", vcode);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, server_url, MethodClass.Json_rpc_format(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
                Log.e("resp", response.toString());

                if (response.has("error")) {
                    editText1.setText("");
                    editText2.setText("");
                    editText3.setText("");
                    editText4.setText("");
                    editText5.setText("");
                    editText6.setText("");
                    editText1.requestFocus();

                }

                JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);
                if (jsonObject != null) {
                    if (type.equals("S")) {
                        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(VerifyActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText(getResources().getString(R.string.verify_success));
                        sweetAlertDialog.setContentText(getResources().getString(R.string.your_email_successfully_verified_login_to_user_continue));
                        sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                       sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(SweetAlertDialog sweetAlertDialog) {
                               sweetAlertDialog.dismissWithAnimation();
                               Intent intent = new Intent(VerifyActivity.this, LoginActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                               startActivity(intent);
                               finish();
                           }
                       }).show();

                    } else if (type.equals("F")) {
                        Intent intent = new Intent(VerifyActivity.this, ResetPasswordActtivity.class);
                        intent.putExtra("email", get_email_from_intent);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
            }
        });
        MySingleton.getInstance(VerifyActivity.this).addToRequestQueue(request);
    }


    private void resendOtp() {
        if (!isNetworkConnected(VerifyActivity.this)) {
            MethodClass.network_error_alert(VerifyActivity.this);
            return;
        }
        MethodClass.showProgressDialog(VerifyActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "resend-code";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email_from_intent);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, server_url, MethodClass.Json_rpc_format(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
                JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);
                try {
                    if (jsonObject != null) {
                       /* otp.setText(jsonObject.getString("vcode"));*/
                      final SweetAlertDialog dialog=  new SweetAlertDialog(VerifyActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                        dialog.setTitleText(getResources().getString(R.string.success));
                        dialog.setContentText(getResources().getString(R.string.please_wait_a_moment_you_will_get_an_OTP_in_your_email));
                        dialog.setConfirmText(getResources().getString(R.string.done));
                        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                });
                        dialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("resp", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
            }
        });
        MySingleton.getInstance(VerifyActivity.this).addToRequestQueue(request);
    }

    public void back(View view) {
        VerifyActivity.super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        String et1 = "", et2 = "", et3 = "", et4 = "", et5 = "", et6 = "";

        et1 = editText1.getText().toString().trim();
        et2 = editText2.getText().toString().trim();
        et3 = editText3.getText().toString().trim();
        et4 = editText4.getText().toString().trim();
        et5 = editText5.getText().toString().trim();
        et6 = editText6.getText().toString().trim();

        if (!et6.equals("")) {
            editText6.setText("");
            editText5.requestFocus();
            return;
        }
        if (!et5.equals("")) {
            editText5.setText("");
            editText4.requestFocus();
            return;
        }
        if (!et4.equals("")) {
            editText4.setText("");
            editText3.requestFocus();
            return;
        }
        if (!et3.equals("")) {
            editText3.setText("");
            editText2.requestFocus();
            return;
        }
        if (!et2.equals("")) {
            editText2.setText("");
            editText1.requestFocus();
            return;
        }
        if (!et1.equals("")) {
            editText1.setText("");
            editText1.requestFocus();
            return;
        }
        super.onBackPressed();
    }
}
