package com.convergentinfoware.gasagricultura.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.Helper.RequestManager;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PICK_FROM_GALLERY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TAKE_PHOTO_CODE;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ProfileEditActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private EditText first_name_et, last_name_et, mobile_et, about_et;
    private ImageView profile_image;
    private LinearLayout upload_img_lay;
    private Button update_btn;

    ///////image////////////
    private Uri outputFileUri;
    private String imageFilePath;
    private String img_path = "";
    ///////image close////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_profile_edit);
        hide_keyboard(this);
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        title.setText(R.string.my_account);
        first_name_et = findViewById(R.id.first_name_et);
        last_name_et = findViewById(R.id.last_name_et);
        mobile_et = findViewById(R.id.mobile_et);
        about_et = findViewById(R.id.about_et);
        profile_image = findViewById(R.id.profile_image);
        upload_img_lay = findViewById(R.id.upload_img_lay);
        update_btn = findViewById(R.id.update_btn);

        getUserDetails();

        upload_img_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickImage();
            }
        });

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileUpdate();
            }
        });
    }


    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    ////////////////////////Q_IMAGE FUNS////////////////////////////////
    private void clickImage() {
        //In this method sho popup to select which functionality to choose your profile pic
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.chose_from_library), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileEditActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    //in this section when choose Take photo opec camera
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //here check CAMERA permission
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            //if not have CAMERA permission to requestPermissions
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                        } else {
                            //if have CAMERA permission call  camera open function
                            cameraIntent();
                        }
                    }

                } else if (items[item].equals(getString(R.string.chose_from_library))) {
                    //in this section when choose from gallery opec gallery
                    try {
                        //here check Storage permission
                        if (ActivityCompat.checkSelfPermission(ProfileEditActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            //if not have Storage permission to requestPermissions
                            ActivityCompat.requestPermissions(ProfileEditActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                        } else {
                            //if have Storage permission call  gallery open function
                            get_intent();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals(getString(R.string.cancel))) {
                    //in this section when choose cancel to close popup
                    dialog.dismiss();
                }
            }
        });
        builder.show();//here show popup


    }

    private void get_intent() {
        //here open Gallery functionality and select picture
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {
        //here open camera functionality and capture picture and get picture path
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (Exception ex) {
            //here Error occurred while creating the File with  show Snackbar
            ex.printStackTrace();
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.image_size_too_large_choose_other_image), Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
        outputFileUri = FileProvider.getUriForFile(ProfileEditActivity.this, "com.convergentinfoware.gasagricultura.fileprovider", photoFile);
        Log.e("outputFileUri", outputFileUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        Log.e("Intentdata", intent.toString());
        startActivityForResult(intent, TAKE_PHOTO_CODE);
    }

    private File createImageFile() throws IOException {
        //here create temp file to hols Camera capture image
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */);

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    //getPath function return selected image path
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        Log.e("LOGURI", uri.getScheme());
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = ProfileEditActivity.this.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        } else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    //here get data(image path) of selected image.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.e("requestCode", String.valueOf(requestCode));
            if (requestCode == PICK_FROM_GALLERY && data != null) {
                //in this section get data of when sected from gallery
                if (!data.getData().getAuthority().equals("com.android.providers.downloads.documents")) {
                    Uri img_uri = data.getData();
                    Bitmap bitmap = null;
                    try {
                        String imagePath = getPath(ProfileEditActivity.this, img_uri);
                        img_path = MethodClass.getRightAngleImage(imagePath);//here final image path
                        File imagefile = new File(img_path);
                        if (imagefile.exists()) {
                            bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                            profile_image.setImageBitmap(bitmap);//set image of design(Image view)
                        }
                    } catch (OutOfMemoryError error) {
                        //here selected big size of image when handling OutOfMemoryError with show popup
                        img_path = "";
                        error.printStackTrace();
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    } catch (Exception e) {
                        //here selected image when any type of issue handling Exception with show popup
                        e.printStackTrace();
                        img_path = "";
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        Log.d("ex", e.getMessage());
                    }
                } else {
                    img_path = "";
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    Log.e("image_uri", "null");
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {
                //in this section get data of when selected Tack Photo(Camera)
                File imagefile = null;
                Bitmap bitmap = null;
                try {
                    img_path = MethodClass.getRightAngleImage(imageFilePath);//here final image path
                    Log.e("imageFilePath", imageFilePath);
                    imagefile = new File(img_path);
                    if (imagefile.exists()) {
                        bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                        profile_image.setImageBitmap(bitmap);//set image of design(Image view)
                    }
                } catch (OutOfMemoryError error) {
                    //here selected big size of image when handling OutOfMemoryError with show popup
                    img_path = "";
                    error.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } catch (Exception e) {
                    //here selected image when any type of issue handling Exception with show popup
                    img_path = "";
                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } else {

            //business_logo_iv.setVisibility(View.GONE);

        }
    }

    //here when user first time give permission to selected image then call onRequestPermissionsResult
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PHOTO_CODE) {
            //in this section get data of when sected tack photo
            //check permission if give permission to open camera otherwise show message "Camera Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();
            } else {
                Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PICK_FROM_GALLERY) {
            //in this section get data of when sected from gallery
            //check permission if give permission to open gallery otherwise show message "Storage Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                get_intent();
            } else {
                Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    //////////////////////////////CLOSE////////////////////////////////


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ProfileEditActivity.this);
        MethodClass.setMenu(ProfileEditActivity.this);
    }


    private void getUserDetails() {
        if (!isNetworkConnected(ProfileEditActivity.this)) {
            MethodClass.network_error_alert(ProfileEditActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProfileEditActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "user-details";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProfileEditActivity.this);
                Log.e("respUser", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProfileEditActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject user = resultResponse.getJSONObject("user");
                        String id = user.getString("id");
                        String first_name = user.getString("first_name");
                        String last_name = user.getString("last_name");
                        String name=first_name+" "+last_name;
                        String mobile = user.getString("mobile");
                        String about = user.getString("about");
                        String profile_img = user.getString("profile_image");
                        String is_seller = user.getString("is_seller");

                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("user_fname", first_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("user_lname", last_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("user_name", name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("user_id", id).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putBoolean("is_logged_in", true).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("profile_pic", profile_img).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("mobile", mobile).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("is_seller", is_seller).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).edit().putString("about", about).commit();


                        first_name_et.setText(first_name);
                        last_name_et.setText(last_name);
                        mobile_et.setText(mobile);
                        if (!about.equals(null) && !about.equals("null") && !about.equals("")){
                            about_et.setText(about);
                        }
                        String profile_im = PROFILE_IMAGE_URL + profile_img;
                        Picasso.get().load(profile_im).placeholder(R.drawable.ic_defoult_image).error(R.drawable.ic_defoult_image).into(profile_image);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ProfileEditActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProfileEditActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProfileEditActivity.this);
                } else {
                    MethodClass.error_alert(ProfileEditActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ProfileEditActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void profileUpdate() {

        String fname, lname, mobile, about;

        fname = first_name_et.getText().toString().trim();
        lname = last_name_et.getText().toString().trim();
        mobile = mobile_et.getText().toString().trim();
        about = about_et.getText().toString().trim();


        if (fname.length() == 0) {
            first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
            first_name_et.requestFocus();
            return;
        }
        if (lname.length() == 0) {
            last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
            last_name_et.requestFocus();
            return;
        }
        if (mobile.length() == 0) {
            mobile_et.setError(getResources().getString(R.string.please_enter_mobile_no));
            mobile_et.requestFocus();
            return;
        }
        if (mobile.length() < 10) {
            mobile_et.setError(getResources().getString(R.string.please_enter_valid_number));
            mobile_et.requestFocus();
            return;
        }
        if (about.length() == 0) {
            about_et.setError(getResources().getString(R.string.please_enter_about));
            about_et.requestFocus();
            return;
        }


        MethodClass.showProgressDialog(ProfileEditActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "user-update";
        Log.e("server_url", server_url);
        SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MethodClass.hideProgressDialog(ProfileEditActivity.this);
                Log.e("onResponse: ", response.toString());
                try {
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ProfileEditActivity.this, new JSONObject(response));
                    if (jsonObject != null) {
                        new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText(jsonObject.getString("message")).setContentText(jsonObject.getString("meaning")).setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                getUserDetails();
                            }
                        }).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MethodClass.error_alert(ProfileEditActivity.this);
                    Log.e("Exception", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ProfileEditActivity.this);
                Log.e("error", error.toString());
                MethodClass.hideProgressDialog(ProfileEditActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProfileEditActivity.this);
                } else {
                    MethodClass.error_alert(ProfileEditActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileEditActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = RequestManager.getnstance(ProfileEditActivity.this);

        simpleMultiPartRequest.addMultipartParam("fname", "text", fname);
        Log.d("fname", fname);
        simpleMultiPartRequest.addMultipartParam("lname", "text", lname);
        Log.d("lname", lname);
        simpleMultiPartRequest.addMultipartParam("mobile", "text", mobile);
        Log.d("mobile", mobile);
        simpleMultiPartRequest.addMultipartParam("about", "text", about);
        Log.d("about", about);
        simpleMultiPartRequest.addMultipartParam("about", "text", about);
        Log.d("about", about);

        if (!img_path.equals("null") && !img_path.equals(null) && !img_path.equals("")) {
            simpleMultiPartRequest.addFile("user_image", img_path);
            Log.e("user_image", img_path);
        }
        simpleMultiPartRequest.setFixedStreamingMode(true);
        int socketTimeout = 100000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
        simpleMultiPartRequest.setRetryPolicy(policy);
        requestQueue.add(simpleMultiPartRequest);
    }


}

