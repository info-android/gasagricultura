package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;


public class ResetPasswordActtivity extends AppCompatActivity {
    private EditText edt_pswrd, edt_conf_pass;
    private Button sbmt_btn;
    private String email = "";
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_reset_password_acttivity);
        MethodClass.hide_keyboard(ResetPasswordActtivity.this);

        edt_pswrd = findViewById(R.id.password_et);
        edt_conf_pass = findViewById(R.id.confirm_password_et);
        sbmt_btn = findViewById(R.id.sbmt_btn);
        title=findViewById(R.id.title);
        title.setText(getResources().getString(R.string.reset_password));
        if (getIntent().getExtras() != null) {
            email = getIntent().getStringExtra("email");
        }

        sbmt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetPassword();
            }
        });
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }

    private void ResetPassword() {
        String get_pswrd = edt_pswrd.getText().toString().trim();
        String get_conf_pswrd = edt_conf_pass.getText().toString().trim();

        if (get_pswrd.length() == 0) {
            edt_pswrd.setError(getResources().getString(R.string.please_enter_password));
            edt_pswrd.requestFocus();
            return;
        }
        if (get_conf_pswrd.length() == 0) {
            edt_conf_pass.setError(getResources().getString(R.string.please_enter_your_confirm_password));
            edt_conf_pass.requestFocus();
            return;
        }
        if (!get_pswrd.equals(get_conf_pswrd)) {
            edt_conf_pass.setError(getResources().getString(R.string.password_is_not_matched_with_confirm_password));
            edt_conf_pass.requestFocus();
            return;
        }
        if (!isNetworkConnected(ResetPasswordActtivity.this)){
            MethodClass.network_error_alert(ResetPasswordActtivity.this);
            return;
        }

        MethodClass.showProgressDialog(ResetPasswordActtivity.this);
        String server_url = getString(R.string.SERVER_URL) + "update-pass";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", get_pswrd);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("jsonObject",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ResetPasswordActtivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ResetPasswordActtivity.this, response);
                    if (jsonObject != null) {

                        String message = jsonObject.getString("message");
                        String meaning = jsonObject.getString("meaning");
                        new SweetAlertDialog(ResetPasswordActtivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message)
                                .setContentText(meaning)
                                .setConfirmText(getResources().getString(R.string.ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent intent = new Intent(ResetPasswordActtivity.this,LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ResetPasswordActtivity.this);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ResetPasswordActtivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ResetPasswordActtivity.this);
                } else {
                    MethodClass.error_alert(ResetPasswordActtivity.this);
                }
            }
        });
        MySingleton.getInstance(ResetPasswordActtivity.this).addToRequestQueue(jsonObjectRequest);

    }

}
