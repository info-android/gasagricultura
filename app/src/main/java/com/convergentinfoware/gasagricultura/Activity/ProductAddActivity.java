package com.convergentinfoware.gasagricultura.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.Helper.RequestManager;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PICK_FROM_GALLERY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TAKE_PHOTO_CODE;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ProductAddActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private EditText product_name_et, product_price_et, product_avi_qty_et, product_desc_et;
    private Spinner product_cat_spin, product_unit_pri_spin;
    private ImageView product_image;
    private LinearLayout choose_product_img_lay;
    private Button add_product_btn;

    ///////image////////////
    private Uri outputFileUri;
    private String imageFilePath;
    private String img_path = "";
    ///////image close////////////



    private ArrayList<MethodClass.StringWithTag> catArrayList, unitArrayList;
    private String product_cat_id = "0", product_unit_id = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_poduct_add);
        hide_keyboard(this);
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        title.setText(R.string.product_add);
        product_name_et = findViewById(R.id.product_name_et);
        product_price_et = findViewById(R.id.product_price_et);
        product_desc_et = findViewById(R.id.product_desc_et);
        product_avi_qty_et = findViewById(R.id.product_avi_qty_et);
        product_cat_spin = findViewById(R.id.product_cat_spin);
        product_unit_pri_spin = findViewById(R.id.product_unit_pri_spin);
        product_image = findViewById(R.id.product_image);
        choose_product_img_lay = findViewById(R.id.choose_product_img_lay);
        add_product_btn = findViewById(R.id.add_product_btn);
        product_catList_unit_Lis();

        product_price_et.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(1)});
        product_avi_qty_et.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(1)});


        choose_product_img_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickImage();
            }
        });

        add_product_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_product_btn.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        add_product_btn.setClickable(true);

                    }
                }, 900);
                addProduct();
            }
        });
    }


    private void product_catList_unit_Lis() {
        if (!isNetworkConnected(ProductAddActivity.this)) {
            MethodClass.network_error_alert(ProductAddActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductAddActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "cat-unit";
        HashMap<String, String> main_param = new HashMap<String, String>();
        main_param.put("jsonrpc", "2.0");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(main_param), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductAddActivity.this);
                Log.e("respCAtUNIT", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductAddActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray categoriesJArray = resultResponse.getJSONArray("categories");
                        catArrayList = new ArrayList<MethodClass.StringWithTag>();
                        catArrayList.add(new MethodClass.StringWithTag("Selected", "0"));
                        for (int i = 0; i < categoriesJArray.length(); i++) {
                            String id = categoriesJArray.getJSONObject(i).getString("id");
                            String title = categoriesJArray.getJSONObject(i).getString("title");
                            catArrayList.add(new MethodClass.StringWithTag(title, id));
                        }
                        ArrayAdapter catArrayAdapter = new ArrayAdapter(ProductAddActivity.this, R.layout.spinner_tv_layout, catArrayList);
                        product_cat_spin.setAdapter(catArrayAdapter);

                        product_cat_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) adapterView.getItemAtPosition(i);
                                product_cat_id = String.valueOf(stringWithTag.tag);
                                if (!product_cat_id.equals("0")){
                                    product_cat_spin.setBackground(getDrawable(R.drawable.spineer_back));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


                        JSONArray unitJArray = resultResponse.getJSONArray("unit");
                        unitArrayList = new ArrayList<MethodClass.StringWithTag>();
                        unitArrayList.add(new MethodClass.StringWithTag("Selected", "0"));
                        for (int i = 0; i < unitJArray.length(); i++) {
                            String id = unitJArray.getJSONObject(i).getString("id");
                            String title = unitJArray.getJSONObject(i).getString("unit");
                            unitArrayList.add(new MethodClass.StringWithTag(title, id));
                        }
                        ArrayAdapter unitArrayAdapter = new ArrayAdapter(ProductAddActivity.this, R.layout.spinner_tv_layout, unitArrayList);
                        product_unit_pri_spin.setAdapter(unitArrayAdapter);

                        product_unit_pri_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) adapterView.getItemAtPosition(i);
                                product_unit_id = String.valueOf(stringWithTag.tag);
                                if (!product_unit_id.equals("0")){
                                    product_unit_pri_spin.setBackground(getDrawable(R.drawable.spineer_back));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ProductAddActivity.this);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ProductAddActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductAddActivity.this);
                } else {
                    MethodClass.error_alert(ProductAddActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductAddActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ProductAddActivity.this).addToRequestQueue(jsonObjectRequest);

    }


    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    ////////////////////////Q_IMAGE FUNS////////////////////////////////
    private void clickImage() {
        //In this method sho popup to select which functionality to choose your profile pic
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.chose_from_library), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProductAddActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    //in this section when choose Take photo opec camera
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //here check CAMERA permission
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            //if not have CAMERA permission to requestPermissions
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                        } else {
                            //if have CAMERA permission call  camera open function
                            cameraIntent();
                        }
                    }

                } else if (items[item].equals(getString(R.string.chose_from_library))) {
                    //in this section when choose from gallery opec gallery
                    try {
                        //here check Storage permission
                        if (ActivityCompat.checkSelfPermission(ProductAddActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            //if not have Storage permission to requestPermissions
                            ActivityCompat.requestPermissions(ProductAddActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                        } else {
                            //if have Storage permission call  gallery open function
                            get_intent();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals(getString(R.string.cancel))) {
                    //in this section when choose cancel to close popup
                    dialog.dismiss();
                }
            }
        });
        builder.show();//here show popup


    }

    private void get_intent() {
        //here open Gallery functionality and select picture
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {
        //here open camera functionality and capture picture and get picture path
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (Exception ex) {
            //here Error occurred while creating the File with  show Snackbar
            ex.printStackTrace();
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.image_size_too_large_choose_other_image), Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
        outputFileUri = FileProvider.getUriForFile(ProductAddActivity.this, "com.convergentinfoware.gasagricultura.fileprovider", photoFile);
        Log.e("outputFileUri", outputFileUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        Log.e("Intentdata", intent.toString());
        startActivityForResult(intent, TAKE_PHOTO_CODE);
    }

    private File createImageFile() throws IOException {
        //here create temp file to hols Camera capture image
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */);

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    //getPath function return selected image path
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        Log.e("LOGURI", uri.getScheme());
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = ProductAddActivity.this.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        } else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    //here get data(image path) of selected image.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.e("requestCode", String.valueOf(requestCode));
            if (requestCode == PICK_FROM_GALLERY && data != null) {
                //in this section get data of when sected from gallery
                if (!data.getData().getAuthority().equals("com.android.providers.downloads.documents")) {
                    Uri img_uri = data.getData();
                    Bitmap bitmap = null;
                    try {
                        String imagePath = getPath(ProductAddActivity.this, img_uri);
                        img_path = MethodClass.getRightAngleImage(imagePath);//here final image path
                        File imagefile = new File(img_path);
                        if (imagefile.exists()) {
                            bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                            product_image.setImageBitmap(bitmap);//set image of design(Image view)
                        }
                    } catch (OutOfMemoryError error) {
                        //here selected big size of image when handling OutOfMemoryError with show popup
                        img_path = "";
                        error.printStackTrace();
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    } catch (Exception e) {
                        //here selected image when any type of issue handling Exception with show popup
                        e.printStackTrace();
                        img_path = "";
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        Log.d("ex", e.getMessage());
                    }
                } else {
                    img_path = "";
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    Log.e("image_uri", "null");
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {
                //in this section get data of when selected Tack Photo(Camera)
                File imagefile = null;
                Bitmap bitmap = null;
                try {
                    img_path = MethodClass.getRightAngleImage(imageFilePath);//here final image path
                    Log.e("imageFilePath", imageFilePath);
                    imagefile = new File(img_path);
                    if (imagefile.exists()) {
                        bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                        product_image.setImageBitmap(bitmap);//set image of design(Image view)
                    }
                } catch (OutOfMemoryError error) {
                    //here selected big size of image when handling OutOfMemoryError with show popup
                    img_path = "";
                    error.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } catch (Exception e) {
                    //here selected image when any type of issue handling Exception with show popup
                    img_path = "";
                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } else {

            //business_logo_iv.setVisibility(View.GONE);

        }

        if (img_path.length()!=0){
            choose_product_img_lay.setBackground(getDrawable(R.drawable.dash_dot));
        }else {
            choose_product_img_lay.setBackground(getDrawable(R.drawable.error_dash_dot));
        }
    }

    //here when user first time give permission to selected image then call onRequestPermissionsResult
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PHOTO_CODE) {
            //in this section get data of when sected tack photo
            //check permission if give permission to open camera otherwise show message "Camera Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();
            } else {
                Toast.makeText(ProductAddActivity.this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PICK_FROM_GALLERY) {
            //in this section get data of when sected from gallery
            //check permission if give permission to open gallery otherwise show message "Storage Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                get_intent();
            } else {
                Toast.makeText(ProductAddActivity.this, getResources().getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    //////////////////////////////CLOSE////////////////////////////////


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ProductAddActivity.this);
        MethodClass.setMenu(ProductAddActivity.this);
    }


    private void addProduct() {

        final String product_name, product_price, product_avi_qty, product_desc;

        product_name = product_name_et.getText().toString().trim();
        product_price = product_price_et.getText().toString().trim().replace(",", ".");
        product_avi_qty = product_avi_qty_et.getText().toString().trim().replace(",", ".");
        product_desc = product_desc_et.getText().toString().trim();

        editTextChengeColor(product_name_et);
        editTextChengeColor(product_price_et);
        editTextChengeColor(product_avi_qty_et);
        boolean isAllFildFill= true;

        if (product_name.length() == 0) {
            /*product_name_et.setError("Please enter product name");
            product_name_et.requestFocus();*/
            product_name_et.setBackground(getDrawable(R.drawable.error_editext_back));
            isAllFildFill=false;
        }

        if (product_cat_id.equals("0")) {
            /*Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product category", Snackbar.LENGTH_SHORT);
            snackbar.show();*/
            product_cat_spin.setBackground(getDrawable(R.drawable.error_spineer_back));
            isAllFildFill=false;
        }

        if (product_price.length() == 0) {
           /* product_price_et.setError("Please enter product price");
            product_price_et.requestFocus();*/
            product_price_et.setBackground(getDrawable(R.drawable.error_editext_back));
            isAllFildFill=false;
        }
        if (product_unit_id.equals("0")) {
            /*Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product unit price", Snackbar.LENGTH_SHORT);
            snackbar.show();*/
            product_unit_pri_spin.setBackground(getDrawable(R.drawable.error_spineer_back));
            isAllFildFill=false;
        }

        if (product_avi_qty.length() == 0) {
            /*product_avi_qty_et.setError("Please enter Available quantity");
            product_avi_qty_et.requestFocus();*/
            product_avi_qty_et.setBackground(getDrawable(R.drawable.error_editext_back));
            isAllFildFill=false;
        }

       /* if (product_desc.length() == 0) {
            product_desc_et.setBackground(getDrawable(R.drawable.error_editext_back));
            isAllFildFill=false;
        }*/

        if (img_path.length() == 0) {
            /*Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product image", Snackbar.LENGTH_SHORT);
            snackbar.show();*/
            choose_product_img_lay.setBackground(getDrawable(R.drawable.error_dash_dot));
            isAllFildFill=false;
        }

        if (!isAllFildFill){
            return;
        }

        if (!isNetworkConnected(ProductAddActivity.this)) {
            MethodClass.network_error_alert(ProductAddActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductAddActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-add";
        Log.e("server_url", server_url);
        SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MethodClass.hideProgressDialog(ProductAddActivity.this);
                Log.e("onResponse: ", response.toString());
                try {
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ProductAddActivity.this, new JSONObject(response));
                    Log.e("jsonObject", jsonObject.toString());
                    if (jsonObject != null) {
                        new SweetAlertDialog(ProductAddActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText(jsonObject.getString("message")).setContentText(jsonObject.getString("meaning")).setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                Intent intent = new Intent(ProductAddActivity.this, ManageProductActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MethodClass.error_alert(ProductAddActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ProductAddActivity.this);
                Log.e("error", error.toString());
                MethodClass.hideProgressDialog(ProductAddActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductAddActivity.this);
                } else {
                    MethodClass.error_alert(ProductAddActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductAddActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = RequestManager.getnstance(ProductAddActivity.this);
        simpleMultiPartRequest.addMultipartParam("product_name", "text", product_name);
        simpleMultiPartRequest.addMultipartParam("cat_id", "text", product_cat_id);
        simpleMultiPartRequest.addMultipartParam("price", "text", product_price);
        simpleMultiPartRequest.addMultipartParam("unit_id", "text", product_unit_id);
        simpleMultiPartRequest.addMultipartParam("quantity", "text", product_avi_qty);
        simpleMultiPartRequest.addMultipartParam("description", "text", product_desc);
        simpleMultiPartRequest.addFile("product_image", img_path);
        simpleMultiPartRequest.setFixedStreamingMode(true);
        int socketTimeout = 100000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
        simpleMultiPartRequest.setRetryPolicy(policy);
        requestQueue.add(simpleMultiPartRequest);
    }


    class DecimalDigitsInputFilter implements InputFilter {
        private final int decimalDigits;

        /**
         * Constructor.
         *
         * @param decimalDigits maximum decimal digits
         */
        public DecimalDigitsInputFilter(int decimalDigits) {
            this.decimalDigits = decimalDigits;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            int dotPos = -1;
            int len = dest.length();
            for (int i = 0; i < len; i++) {
                char c = dest.charAt(i);
                if (c == '.' || c == ',') {
                    dotPos = i;
                    break;
                }
            }
            if (dotPos >= 0) {
                // protects against many dots
                if (source.equals(".") || source.equals(",")) {
                    return "";
                }
                // if the text is entered before the dot
                if (dend <= dotPos) {
                    return null;
                }
                if (len - dotPos > decimalDigits) {
                    return "";
                }
            }
            return null;
        }
    }

    private void editTextChengeColor(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editText.getText().toString().trim().length() == 0) {
                    editText.setBackground(getDrawable(R.drawable.error_editext_back));
                }else {
                    editText.setBackground(getDrawable(R.drawable.editext_back));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

}




































/*if (product_name.length() == 0) {
        product_name_et.setError("Please enter product name");
        product_name_et.requestFocus();
        return;
        }

        if (product_cat_id.equals("0")) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product category", Snackbar.LENGTH_SHORT);
        snackbar.show();
        return;
        }

        if (product_price.length() == 0) {
        product_price_et.setError("Please enter product price");
        product_price_et.requestFocus();

        return;
        }
        if (product_unit_id.equals("0")) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product unit price", Snackbar.LENGTH_SHORT);
        snackbar.show();
        return;
        }

        if (product_avi_qty.length() == 0) {
        product_avi_qty_et.setError("Please enter Available quantity");
        product_avi_qty_et.requestFocus();
        //product_name_et.setBackground(getDrawable(R.drawable.bacground_to_change_color));
        return;
        }
        if (product_desc.length() == 0) {
        product_desc_et.setError("Please enter product description");
        product_desc_et.requestFocus();
        return;
        }

        if (img_path.length() == 0) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add product image", Snackbar.LENGTH_SHORT);
        snackbar.show();
        return;
        }

        if (!isNetworkConnected(ProductAddActivity.this)) {
        MethodClass.network_error_alert(ProductAddActivity.this);
        return;
        }*/
