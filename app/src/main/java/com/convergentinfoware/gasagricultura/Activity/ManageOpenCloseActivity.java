package com.convergentinfoware.gasagricultura.Activity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Helper.DurationTimePickDialog;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ManageOpenCloseActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private Spinner from_day_spin, to_day_spin, dispatch_day_spin;
    private TextView from_time_tv, to_time_tv, dispatch_time_tv;
    private RadioButton open_rbtn_yes, open_rbtn_no, close_rbtn_yes, close_rbtn_no,regu_rbtn_yes,regu_rbtn_no,arch_only_rbtn_yes,arch_only_rbtn_no;
    private Button save_btn;
    private String[] days = {};

    ArrayList<MethodClass.StringWithTag> withTagArrayList;
    ArrayAdapter daysArrayAdapter;
    EditText archived_day_et;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_manage_open_close);
        hide_keyboard(this);
        days = new String[]{"Selezionata",  getResources().getString(R.string.monday),
                getResources().getString(R.string.tuesday),
                getResources().getString(R.string.wednesday),
                getResources().getString(R.string.thursday),
                getResources().getString(R.string.friday),
                getResources().getString(R.string.saturday),
                getResources().getString(R.string.sunday)
        };
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        title.setText(R.string.manage_open_close);

        from_day_spin = findViewById(R.id.from_day_spin);
        to_day_spin = findViewById(R.id.to_day_spin);
        dispatch_day_spin = findViewById(R.id.dispatch_day_spin);
        from_time_tv = findViewById(R.id.from_time_tv);
        to_time_tv = findViewById(R.id.to_time_tv);
        dispatch_time_tv = findViewById(R.id.dispatch_time_tv);
        open_rbtn_yes = findViewById(R.id.open_rbtn_yes);
        open_rbtn_no = findViewById(R.id.open_rbtn_no);
        close_rbtn_yes = findViewById(R.id.close_rbtn_yes);
        close_rbtn_no = findViewById(R.id.close_rbtn_no);
        regu_rbtn_yes = findViewById(R.id.regu_rbtn_yes);
        regu_rbtn_no = findViewById(R.id.regu_rbtn_no);
        archived_day_et = findViewById(R.id.archived_day_et);
        arch_only_rbtn_yes = findViewById(R.id.arch_only_rbtn_yes);
        arch_only_rbtn_no = findViewById(R.id.arch_only_rbtn_no);
        save_btn = findViewById(R.id.save_btn);

        withTagArrayList = new ArrayList<>();
        for (int i = 0; i < days.length; i++) {
            withTagArrayList.add(new MethodClass.StringWithTag(days[i], i));
        }
        daysArrayAdapter = new ArrayAdapter(ManageOpenCloseActivity.this, R.layout.spinner_tv_layout, withTagArrayList);
        from_day_spin.setAdapter(daysArrayAdapter);
        to_day_spin.setAdapter(daysArrayAdapter);
        dispatch_day_spin.setAdapter(daysArrayAdapter);

        from_time_tv.setText("8:30");
        to_time_tv.setText("8:30");
        dispatch_time_tv.setText("8:30");

        get_contents();


        open_rbtn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (open_rbtn_yes.isChecked()){
                    open_rbtn_no.setChecked(false);

                    close_rbtn_yes.setChecked(false);
                    close_rbtn_no.setChecked(true);

                    regu_rbtn_yes.setChecked(false);
                    regu_rbtn_no.setChecked(true);
                }
            }
        });

        open_rbtn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (open_rbtn_no.isChecked()){
                    open_rbtn_yes.setChecked(false);
                }
            }
        });

        close_rbtn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (close_rbtn_yes.isChecked()){
                    open_rbtn_yes.setChecked(false);
                    open_rbtn_no.setChecked(true);

                    close_rbtn_no.setChecked(false);

                    regu_rbtn_yes.setChecked(false);
                    regu_rbtn_no.setChecked(true);
                }
            }
        });

        close_rbtn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (close_rbtn_no.isChecked()){
                    close_rbtn_yes.setChecked(false);
                }
            }
        });

        regu_rbtn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (regu_rbtn_yes.isChecked()){
                    open_rbtn_yes.setChecked(false);
                    open_rbtn_no.setChecked(true);
                    close_rbtn_yes.setChecked(false);
                    close_rbtn_no.setChecked(true);
                    regu_rbtn_no.setChecked(false);
                }

            }
        });

        regu_rbtn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (regu_rbtn_no.isChecked()){
                    open_rbtn_yes.setChecked(false);
                    open_rbtn_no.setChecked(true);
                    close_rbtn_yes.setChecked(true);
                    close_rbtn_no.setChecked(false);
                    regu_rbtn_yes.setChecked(false);
                }

            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateShopTime();
            }
        });


    }


    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ManageOpenCloseActivity.this);
        MethodClass.setMenu(ManageOpenCloseActivity.this);
    }


    private void updateShopTime() {
        if (!isNetworkConnected(ManageOpenCloseActivity.this)) {
            MethodClass.network_error_alert(ManageOpenCloseActivity.this);
            return;
        }
        if (from_day_spin.getSelectedItemPosition()==0){
            Snackbar.make(findViewById(android.R.id.content),getResources().getString(R.string.please_select_from_day),Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (to_day_spin.getSelectedItemPosition()==0){
            Snackbar.make(findViewById(android.R.id.content),getResources().getString(R.string.please_select_to_day),Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (dispatch_day_spin.getSelectedItemPosition()==0){
            Snackbar.make(findViewById(android.R.id.content),getResources().getString(R.string.please_select_dispatch_day),Snackbar.LENGTH_SHORT).show();
            return;
        }
       /* if (from_day_spin.getSelectedItemPosition()>to_day_spin.getSelectedItemPosition()){
            Snackbar.make(findViewById(android.R.id.content),"Please select smaller From day from To day",Snackbar.LENGTH_SHORT).show();
            return;
        }*/

        if (archived_day_et.getText().toString().trim().length() == 0){
            archived_day_et.setError(getResources().getString(R.string.please_enter_archived_day));
            archived_day_et.requestFocus();
            return;
        }


        MethodClass.showProgressDialog(ManageOpenCloseActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "shop-time-update";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("from_day", String.valueOf(from_day_spin.getSelectedItemPosition()));
        params.put("from_time", from_time_tv.getText().toString().trim());
        params.put("to_day", String.valueOf(to_day_spin.getSelectedItemPosition()));
        params.put("to_time", to_time_tv.getText().toString().trim());
        params.put("dispach_day", String.valueOf(dispatch_day_spin.getSelectedItemPosition()));
        params.put("dispach_time", dispatch_time_tv.getText().toString().trim());
        params.put("force_open", open_rbtn_yes.isChecked() == true ? "Y" : "N");
        params.put("force_close", close_rbtn_yes.isChecked() == true ? "Y" : "N");
        params.put("regular_schedule", regu_rbtn_yes.isChecked() == true ? "Y" : "N");
        params.put("archived", arch_only_rbtn_yes.isChecked() == true ? "Y" : "N");
        params.put("arc_day", archived_day_et.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("jsonObject",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ManageOpenCloseActivity.this);
                Log.e("respUser", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ManageOpenCloseActivity.this, response);
                    if (resultResponse != null) {
                        new SweetAlertDialog(ManageOpenCloseActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText(resultResponse.getString("message")).setContentText(resultResponse.getString("meaning")).setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ManageOpenCloseActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ManageOpenCloseActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ManageOpenCloseActivity.this);
                } else {
                    MethodClass.error_alert(ManageOpenCloseActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ManageOpenCloseActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ManageOpenCloseActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void get_contents() {
        if (!isNetworkConnected(ManageOpenCloseActivity.this)) {
            MethodClass.network_error_alert(ManageOpenCloseActivity.this);
            return;
        }

        MethodClass.showProgressDialog(ManageOpenCloseActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "shop-content";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ManageOpenCloseActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ManageOpenCloseActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject shoptime = resultResponse.getJSONObject("shoptime");
                        PreferenceManager.getDefaultSharedPreferences(ManageOpenCloseActivity.this).edit().putString("shoptime", String.valueOf(shoptime)).commit();

                        Integer from_day = Integer.valueOf(shoptime.getString("from_day"));
                        from_day_spin.setSelection(from_day);
                        Integer to_day = Integer.valueOf(shoptime.getString("to_day"));
                        to_day_spin.setSelection(to_day);
                        Integer dispach_day = Integer.valueOf(shoptime.getString("dispach_day"));
                        dispatch_day_spin.setSelection(dispach_day);
                        String arc_day=shoptime.getString("arc_day");
                        archived_day_et.setText(arc_day);


                        String from_time = shoptime.getString("from_time");
                        String to_time = shoptime.getString("to_time");
                        String dispach_time = shoptime.getString("dispach_time");
                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        SimpleDateFormat printdateFormat = new SimpleDateFormat("H:mm");
                        final Date fromtime = dateFormat.parse(from_time);
                        final Date totime = dateFormat.parse(to_time);
                        final Date dispatchtime = dateFormat.parse(dispach_time);
                        from_time_tv.setText(printdateFormat.format(fromtime));
                        to_time_tv.setText(printdateFormat.format(totime));
                        dispatch_time_tv.setText(printdateFormat.format(dispatchtime));

                        from_time_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                time(from_time_tv,fromtime.getHours(),fromtime.getMinutes());
                            }
                        });
                        to_time_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                time(to_time_tv,totime.getHours(),totime.getMinutes());
                            }
                        });
                        dispatch_time_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                time(dispatch_time_tv,dispatchtime.getHours(),dispatchtime.getMinutes());
                            }
                        });

                        String force_open=shoptime.getString("force_open");
                        String force_close=shoptime.getString("force_close");
                        String regular_schedule=shoptime.getString("regular_schedule");
                        String archived=shoptime.getString("archived");
                        if (archived.equals("Y")){
                            arch_only_rbtn_yes.setChecked(true);
                            arch_only_rbtn_no.setChecked(false);

                        }else {
                            arch_only_rbtn_yes.setChecked(false);
                            arch_only_rbtn_no.setChecked(true);
                        }

                        if (force_open.equals("Y")){
                            open_rbtn_yes.setChecked(true);
                            open_rbtn_no.setChecked(false);
                        }else {
                            open_rbtn_yes.setChecked(false);
                            open_rbtn_no.setChecked(true);
                        }
                        if (force_close.equals("Y")){
                            close_rbtn_yes.setChecked(true);
                            close_rbtn_no.setChecked(false);
                        }else {
                            close_rbtn_yes.setChecked(false);
                            close_rbtn_no.setChecked(true);
                        }

                        if (regular_schedule.equals("Y")){
                            regu_rbtn_yes.setChecked(true);
                            regu_rbtn_no.setChecked(false);
                        }else {
                            regu_rbtn_yes.setChecked(false);
                            regu_rbtn_no.setChecked(true);
                        }


                    }
                } catch (Exception e) {
                    MethodClass.error_alert(ManageOpenCloseActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );
                MethodClass.hideProgressDialog(ManageOpenCloseActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ManageOpenCloseActivity.this);
                } else {
                    MethodClass.error_alert(ManageOpenCloseActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ManageOpenCloseActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ManageOpenCloseActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    private void time(final TextView textView, int hour, int minute) {
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(ManageOpenCloseActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (selectedMinute>0 && selectedMinute <=30){
                    selectedMinute=30;
                }else {
                    selectedMinute=00;
                }
                String time = selectedHour + ":" + selectedMinute;
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("H:m");
                    SimpleDateFormat printdateFormat = new SimpleDateFormat("H:mm");
                    Date date= dateFormat.parse(time);
                    String output_time= printdateFormat.format(date);
                    textView.setText(output_time);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle(getResources().getString(R.string.select_time));
        mTimePicker.show();


    }

}


  /*DurationTimePickDialog durationTimePickDialog =new DurationTimePickDialog(ManageOpenCloseActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String time = selectedHour + ":" + selectedMinute;
                textView.setText(time);
            }
        },hour, minute, true,30);
        durationTimePickDialog.setTitle("Select Time");
        durationTimePickDialog.show();*/
        /* if (selectedMinute>0 && selectedMinute <=30){
                    selectedMinute=30;
                }else if (selectedMinute>30 && selectedMinute>60){
                    selectedMinute=00;
                    if (selectedHour==0){
                        if (selectedMinute>0){
                            selectedHour=selectedHour+1;
                        }
                    }else {
                        selectedHour=selectedHour+1;
                    }
                }*/
                /*if (selectedHour==0){
                    selectedHour=00;
                }else {
                    selectedHour=selectedHour+1;
                    selectedMinute=00;
                }*/