package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.OrderDetailsAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PEODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ByOrderDetailsActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    private TextView order_no_tv,user_name_tv,order_date_tv,order_price_tv;
    private LinearLayout no_data_lay;
    private NestedScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_order_details);
        drawer_layout=findViewById(R.id.drawer_layout);
        recy_view=findViewById(R.id.recy_view);
        scrollView=findViewById(R.id.scrollView);
        title=findViewById(R.id.title);
        title.setText(R.string.order_details);

        order_no_tv=findViewById(R.id.order_no_tv);
        user_name_tv=findViewById(R.id.user_name_tv);
        order_date_tv=findViewById(R.id.order_date_tv);
        order_price_tv=findViewById(R.id.order_price_tv);
        no_data_lay=findViewById(R.id.no_data_lay);


        String order_id=getIntent().getStringExtra("order_id");

        get_list(order_id);
    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    } public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ByOrderDetailsActivity.this);
        MethodClass.setMenu(ByOrderDetailsActivity.this);

    }

    public void get_list(String order_id) {
        if (!isNetworkConnected(ByOrderDetailsActivity.this)) {
            MethodClass.network_error_alert(ByOrderDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ByOrderDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_id",order_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ByOrderDetailsActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ByOrderDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray order_details = resultResponse.getJSONArray("order_details");
                        if (order_details.length() > 0) {
                            JSONObject order=resultResponse.getJSONObject("order");
                            String order_id=order.getString("id");
                            String order_no=order.getString("order_no");
                            String order_date=order.getString("order_date");
                            String order_total=order.getString("order_total");
                            String first_name=order.getJSONObject("order_customer").getString("first_name");
                            String last_name=order.getJSONObject("order_customer").getString("last_name");
                            String profile_image=order.getJSONObject("order_customer").getString("profile_image");
                            order_no_tv.setText(getResources().getString(R.string.order_no_with_colon)+order_no);
                            user_name_tv.setText(first_name+" "+last_name);
                            order_price_tv.setText(order_total.replace(".",",")+" €");
                            try{
                                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
                                Date date= dateFormat.parse(order_date);
                                order_date_tv.setText(DateFormat.format("dd MMM, yyyy",date));
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < order_details.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                String price = order_details.getJSONObject(i).getString("price");
                                String qty = order_details.getJSONObject(i).getString("qty");
                                String id = order_details.getJSONObject(i).getJSONObject("order_details_product").getString("id");
                                String title = order_details.getJSONObject(i).getJSONObject("order_details_product").getString("title");
                                String description = order_details.getJSONObject(i).getJSONObject("order_details_product").getString("description");
                                String product_image = order_details.getJSONObject(i).getJSONObject("order_details_product").getString("product_image");

                                JSONObject product_user=order_details.getJSONObject(i).getJSONObject("order_details_product").getJSONObject("product_user");
                                String  order_user_id=product_user.getString("id");
                                String  order_user_first_name=product_user.getString("first_name");
                                String  order_user_last_name=product_user.getString("last_name");
                                String  order_user_profile_image=product_user.getString("profile_image");
                                JSONObject order_details_unit=order_details.getJSONObject(i).getJSONObject("order_details_unit");
                                String product_unit=order_details_unit.getString("unit");
                                map.put(ID, id);
                                map.put(TITLE, title);
                                map.put(ORDER_DATE, order_date);
                                map.put(ORDER_TOTAL, order_total);
                                map.put(PRODUCT_PRICE, price);
                                map.put(QTY, qty);
                                map.put(PEODUCT_UNIT, product_unit);
                                map.put(PRODCT_IMAGE, product_image);
                                map.put(USER_ID, order_user_id);
                                map.put(USER_NAME, order_user_first_name + " " + order_user_last_name);
                                map.put(USER_IMAGE, order_user_profile_image);
                                map_list.add(map);
                            }
                            OrderDetailsAdapter orderDetailsAdapter = new OrderDetailsAdapter(ByOrderDetailsActivity.this, map_list);
                            recy_view.setAdapter(orderDetailsAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                            scrollView.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(ByOrderDetailsActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    ByOrderDetailsActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ByOrderDetailsActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(ByOrderDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("err",e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ByOrderDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ByOrderDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ByOrderDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ByOrderDetailsActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ByOrderDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
