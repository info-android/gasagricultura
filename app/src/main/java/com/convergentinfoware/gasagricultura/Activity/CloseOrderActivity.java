package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.ByFarmerAdapter;
import com.convergentinfoware.gasagricultura.Adapter.ByOrderListAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NO_OF_ITEMS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_NO;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.SELLER_ORDER;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.STATUS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TOTAL_WEIGHT_TXT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class CloseOrderActivity extends AppCompatActivity {
    private LinearLayout farmer_layout,product;
    private DrawerLayout drawer_layout;
    private TextView title;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    private LinearLayout no_data_lay;
    private LinearLayout by_farmer_lin,by_order_lin;
    private LinearLayout space_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_close_order);
        drawer_layout=findViewById(R.id.drawer_layout);
        title=findViewById(R.id.title);
        recy_view=findViewById(R.id.recy_view);
        no_data_lay=findViewById(R.id.no_data_lay);
        by_farmer_lin=findViewById(R.id.by_farmer_lin);
        by_order_lin=findViewById(R.id.by_order_lin);
        space_lay=findViewById(R.id.space_lay);
        title=findViewById(R.id.title);
        //title.setText(R.string.close_order);
        shoptime();
        byOrderList();

        by_order_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                space_lay.setVisibility(View.GONE);
                by_farmer_lin.setBackground(getDrawable(R.drawable.farmer_grey));
                by_order_lin.setBackground(getDrawable(R.drawable.by_order_green2));
                byOrderList();
            }
        });
        by_farmer_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                space_lay.setVisibility(View.VISIBLE);
                by_farmer_lin.setBackground(getDrawable(R.drawable.farmer_green));
                by_order_lin.setBackground(getDrawable(R.drawable.by_order_grey2));
                byFarmerList();
            }
        });

    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }
    public void back(View view) {
       super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(CloseOrderActivity.this);
        MethodClass.setMenu(CloseOrderActivity.this);
    }

    public void byOrderList() {
        if (!isNetworkConnected(CloseOrderActivity.this)) {
            MethodClass.network_error_alert(CloseOrderActivity.this);
            return;
        }
        recy_view.setAdapter(null);
        MethodClass.showProgressDialog(CloseOrderActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-list";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(CloseOrderActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(CloseOrderActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray orders = resultResponse.getJSONArray("orders");
                        if (orders.length() > 0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < orders.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                String id = orders.getJSONObject(i).getString("id");
                                String order_no = orders.getJSONObject(i).getString("order_no");
                                String total_weight_txt = orders.getJSONObject(i).getString("total_weight_txt");
                                String order_date = orders.getJSONObject(i).getString("order_date");
                                String order_total = orders.getJSONObject(i).getString("order_total");
                                String status = orders.getJSONObject(i).getString("status");
                                String no_of_items = orders.getJSONObject(i).getString("no_of_items");
                                String order_user_id = orders.getJSONObject(i).getJSONObject("order_customer").getString("id");
                                String order_user_first_name = orders.getJSONObject(i).getJSONObject("order_customer").getString("first_name");
                                String order_user_last_name = orders.getJSONObject(i).getJSONObject("order_customer").getString("last_name");
                                String order_user_profile_image = orders.getJSONObject(i).getJSONObject("order_customer").getString("profile_image");
                                map.put(ID, id);
                                map.put(ORDER_NO, order_no);
                                map.put(ORDER_DATE, order_date);
                                map.put(ORDER_TOTAL, order_total);
                                map.put(TOTAL_WEIGHT_TXT, total_weight_txt);
                                map.put(STATUS, status);
                                map.put(NO_OF_ITEMS, no_of_items);
                                map.put(USER_ID, order_user_id);
                                map.put(USER_NAME, order_user_first_name + " " + order_user_last_name);
                                map.put(USER_IMAGE, order_user_profile_image);
                                map_list.add(map);
                            }
                            ByOrderListAdapter byOrderListAdapter = new ByOrderListAdapter(CloseOrderActivity.this, map_list);
                            recy_view.setAdapter(byOrderListAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(CloseOrderActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    CloseOrderActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    CloseOrderActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(CloseOrderActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(CloseOrderActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(CloseOrderActivity.this);
                } else {
                    MethodClass.error_alert(CloseOrderActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CloseOrderActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(CloseOrderActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void byFarmerList() {
        if (!isNetworkConnected(CloseOrderActivity.this)) {
            MethodClass.network_error_alert(CloseOrderActivity.this);
            return;
        }
        recy_view.setAdapter(null);
        MethodClass.showProgressDialog(CloseOrderActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-closed-list-farmer";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(CloseOrderActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(CloseOrderActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray orders = resultResponse.getJSONArray("orders");
                        if (orders.length() > 0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < orders.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                String id = orders.getJSONObject(i).getJSONObject("seller").getString("id");
                                String first_name = orders.getJSONObject(i).getJSONObject("seller").getString("first_name");
                                String last_name = orders.getJSONObject(i).getJSONObject("seller").getString("last_name");
                                String profile_image = orders.getJSONObject(i).getJSONObject("seller").getString("profile_image");
                                String seller_order = orders.getJSONObject(i).getJSONObject("seller").getString("seller_order");
                                String name = first_name + " " + last_name;
                                map.put(ID, id);
                                map.put(NAME, name);
                                map.put(PROFILE_IMAGE, profile_image);
                                map.put(SELLER_ORDER,seller_order);
                                map_list.add(map);
                            }
                            ByFarmerAdapter byFarmerAdapter = new ByFarmerAdapter(CloseOrderActivity.this, map_list);
                            recy_view.setAdapter(byFarmerAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(CloseOrderActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    CloseOrderActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    CloseOrderActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(CloseOrderActivity.this);
                    e.printStackTrace();
                    //Log.e("Exception",e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("Error",error.toString());
                MethodClass.hideProgressDialog(CloseOrderActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(CloseOrderActivity.this);
                } else {
                    MethodClass.error_alert(CloseOrderActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CloseOrderActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(CloseOrderActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void shoptime() {
        String shoptime_str = PreferenceManager.getDefaultSharedPreferences(CloseOrderActivity.this).getString("shoptime", "");
        try {
            JSONObject shoptime = new JSONObject(shoptime_str);
            String dispach_time = shoptime.getString("dispach_time");
            Integer dispach_day = Integer.valueOf(shoptime.getString("dispach_day"));

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat printdateFormat = new SimpleDateFormat("HH:mm");
           /* String[] days = {"Days", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};*/

            String[] days = {"Days", getResources().getString(R.string.monday),
                    getResources().getString(R.string.tuesday),
                    getResources().getString(R.string.wednesday),
                    getResources().getString(R.string.thursday),
                    getResources().getString(R.string.friday),
                    getResources().getString(R.string.saturday),
                    getResources().getString(R.string.sunday)
            };
            Date fromtime = dateFormat.parse(dispach_time);
            String settitle = getResources().getString(R.string.dispatch )+" "+ days[dispach_day] + " " + printdateFormat.format(fromtime);
            title.setText(settitle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
