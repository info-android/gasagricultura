package com.convergentinfoware.gasagricultura.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.CategoryAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.hide_keyboard;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class FilterActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> catArrayList;
    public static ArrayList<Integer> CAT_CHECK_LIST;
    //private ImageView clear_img;
    //private Button apply_filter_btn;
    private EditText type_et;
    private LinearLayout no_data_lay;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_filter);
        hide_keyboard(this);
        drawer_layout = findViewById(R.id.drawer_layout);
        title = findViewById(R.id.title);
        title.setText(R.string.category);
        recy_view = findViewById(R.id.recy_view);
        //clear_img = findViewById(R.id.clear_img);
        //apply_filter_btn = findViewById(R.id.apply_filter_btn);
        type_et = findViewById(R.id.type_et);
        no_data_lay = findViewById(R.id.no_data_lay);

        product_catList_unit_Lis();

       /* clear_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAT_CHECK_LIST.clear();
                type_et.setText("");
                type_et.setText("");
                product_catList_unit_Lis();
            }
        });
        CAT_CHECK_LIST=new ArrayList<>();

        apply_filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyFilter();
            }
        });*/

        type_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    Intent intent = new Intent(FilterActivity.this, ProductActivity.class);
                    String kets = type_et.getText().toString().trim();
                    intent.putExtra("cat_id", "");
                    intent.putExtra("key", kets);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
    }


    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(FilterActivity.this);
        MethodClass.setMenu(FilterActivity.this);

    }

    private void product_catList_unit_Lis() {
        if (!isNetworkConnected(FilterActivity.this)) {
            MethodClass.network_error_alert(FilterActivity.this);
            return;
        }

        MethodClass.showProgressDialog(FilterActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "cat-unit";
        HashMap<String, String> main_param = new HashMap<String, String>();
        main_param.put("jsonrpc", "2.0");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(main_param), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(FilterActivity.this);
                Log.e("respCAtUNIT", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(FilterActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray categoriesJArray = resultResponse.getJSONArray("categories");
                        if (categoriesJArray.length() > 0) {
                            catArrayList = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < categoriesJArray.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                String id = categoriesJArray.getJSONObject(i).getString("id");
                                String title = categoriesJArray.getJSONObject(i).getString("title");
                                String picture = categoriesJArray.getJSONObject(i).getString("picture");
                                map.put(ID, id);
                                map.put(TITLE, title);
                                map.put(IMAGE, picture);
                                catArrayList.add(map);
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put(ID, "all");
                            map.put(TITLE, getResources().getString(R.string.all));
                            map.put(IMAGE, "all");
                            catArrayList.add(map);
                            CategoryAdapter categoryAdapter = new CategoryAdapter(FilterActivity.this, catArrayList);
                            recy_view.setAdapter(categoryAdapter);//here set adapter in RecyclerView(List view)
                            recy_view.setFocusable(false);
                            recy_view.setVisibility(View.VISIBLE);
                            no_data_lay.setVisibility(View.GONE);
                        } else {
                            recy_view.setVisibility(View.GONE);
                            no_data_lay.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(FilterActivity.this);
                    e.printStackTrace();
                    recy_view.setVisibility(View.GONE);
                    no_data_lay.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                recy_view.setVisibility(View.GONE);
                no_data_lay.setVisibility(View.VISIBLE);
                MethodClass.hideProgressDialog(FilterActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(FilterActivity.this);
                } else {
                    MethodClass.error_alert(FilterActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FilterActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(FilterActivity.this).addToRequestQueue(jsonObjectRequest);

    }


      /* private void applyFilter() {
        if (CAT_CHECK_LIST.size()==0 && type_et.getText().toString().trim().length()==0){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please enter key or select category", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        if (!isNetworkConnected(FilterActivity.this)){
            MethodClass.network_error_alert(FilterActivity.this);
            return;
        }
        Intent intent=new Intent(FilterActivity.this,ProductActivity.class);
        String kets=type_et.getText().toString().trim();
        intent.putExtra("cat_id", TextUtils.join(",", CAT_CHECK_LIST));
        intent.putExtra("key",kets);
        startActivity(intent);
    }*/


}
