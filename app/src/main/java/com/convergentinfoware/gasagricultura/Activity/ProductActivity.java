package com.convergentinfoware.gasagricultura.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Adapter.ProductAdapter;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import io.fabric.sdk.android.Fabric;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_PRODUCTS;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ProductActivity extends AppCompatActivity {
    private DrawerLayout drawer_layout;
    private TextView title;
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> map_list;
    private String key = "";
    private String cat_ids = "";
    private ImageView cart_img;
    private LinearLayout no_data_lay;
    boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_product);
        drawer_layout = findViewById(R.id.drawer_layout);
        recy_view = findViewById(R.id.recy_view);
        title = findViewById(R.id.title);

        no_data_lay = findViewById(R.id.no_data_lay);
        title.setText(R.string.products);

        /*cart_img=findViewById(R.id.cart_img);
        cart_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Intent intent=new Intent(ProductActivity.this,MyShoppingBagActivity.class);
                startActivity(intent);*//*
            }
        });*/

    }

    public void open_nav(View view) {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    private void get_list(String key, String cat_ids) {
        if (!isNetworkConnected(ProductActivity.this)) {
            MethodClass.network_error_alert(ProductActivity.this);
            return;
        }
        String user_id = PreferenceManager.getDefaultSharedPreferences(ProductActivity.this).getString("user_id", "");
        MethodClass.showProgressDialog(ProductActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-list-farmer";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("keyword", key);
        params.put("cat_id", cat_ids.equals("all") ? "" : cat_ids);
        params.put("user_id", "");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("parames", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductActivity.this);
                Log.e("resp", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray product_list = resultResponse.getJSONArray("product_list");
                        if (product_list.length() > 0) {
                            map_list = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < product_list.length(); i++) {
                                JSONObject object = product_list.getJSONObject(i);
                                JSONObject user_data = object.getJSONObject("user_data");
                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(ID, user_data.getString("id"));
                                String first_name = user_data.getString("first_name");
                                String last_name = user_data.getString("last_name");
                                String profile_image = user_data.getString("profile_image");
                                String name = first_name + " " + last_name;
                                map.put(NAME, name);
                                map.put(PROFILE_IMAGE, profile_image);
                                map.put(USER_PRODUCTS, user_data.getString("user_products"));
                                map_list.add(map);
                            }
                            ProductAdapter productAdapter = new ProductAdapter(ProductActivity.this, map_list);
                            recy_view.setAdapter(productAdapter);
                            no_data_lay.setVisibility(View.GONE);
                            recy_view.setVisibility(View.VISIBLE);
                        } else {
                            isBack = true;
                            Log.e("true", "true");
                            no_data_lay.setVisibility(View.VISIBLE);
                            recy_view.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(ProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    ProductActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                  ProductActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }

                } catch (JSONException e) {
                    MethodClass.error_alert(ProductActivity.this);
                    e.printStackTrace();
                    no_data_lay.setVisibility(View.VISIBLE);
                    recy_view.setVisibility(View.GONE);
                    //Log.e("productError",e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );

                no_data_lay.setVisibility(View.VISIBLE);
                recy_view.setVisibility(View.GONE);
                MethodClass.hideProgressDialog(ProductActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductActivity.this);
                } else {
                    MethodClass.error_alert(ProductActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.cartCalculation(ProductActivity.this);
        if (getIntent().getExtras() != null) {
            key = getIntent().getStringExtra("key");
            cat_ids = getIntent().getStringExtra("cat_id");
            Log.e("key", key);
            Log.e("cat_id", cat_ids);
        }
        get_list(key, cat_ids);

        MethodClass.setMenu(ProductActivity.this);
    }

   /* @Override
    public void onBackPressed() {
        Log.e("back2", "back2");
        if (isBack) {
            Log.e("back", "back");
            isBack = false;
            super.onBackPressed();
        }
    }*/

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e("back2", String.valueOf(keyCode));
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("back2", "back2");
            super.onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }*/
}
