package com.convergentinfoware.gasagricultura.Adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.FarmerProductProfileActivity;
import com.convergentinfoware.gasagricultura.Activity.ProductActivity;
import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.DESCRIPTION;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_PRODUCTS;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ProductAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.product_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.name_tv.setText(map.get(NAME));
        String profile_image = PROFILE_IMAGE_URL + map.get(PROFILE_IMAGE);
        Picasso.get().load(profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.user_image);
        get_list(map.get(ID),map.get(NAME),map.get(PROFILE_IMAGE),holder.sub_recy_view,map.get(USER_PRODUCTS));

        holder.farmer_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, FarmerProductProfileActivity.class);
                intent.putExtra("user_id",map.get(ID));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView sub_recy_view;
        private CircleImageView user_image;
        private TextView name_tv;
        private LinearLayout farmer_layout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sub_recy_view=itemView.findViewById(R.id.sub_recy_view);
            user_image=itemView.findViewById(R.id.user_image);
            name_tv=itemView.findViewById(R.id.name_tv);
            farmer_layout=itemView.findViewById(R.id.farmer_layout);
        }
    }
    private void get_list(String user_id,String user_name,String user_img,RecyclerView recyclerView, String user_products) {
        try {
            JSONArray jsonArray=new JSONArray(user_products);
            ArrayList<HashMap<String,String>> hashMapArrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(USER_ID, user_id);
                map.put(USER_NAME, user_name);
                map.put(USER_IMAGE, user_img);
                map.put(ID, jsonArray.getJSONObject(i).getString("id"));
                map.put(TITLE, jsonArray.getJSONObject(i).getString("title"));
                map.put(DESCRIPTION, jsonArray.getJSONObject(i).getString("description"));
                map.put(PRODCT_IMAGE, jsonArray.getJSONObject(i).getString("product_image"));
                map.put(PRODUCT_PRICE, jsonArray.getJSONObject(i).getString("product_price"));
                hashMapArrayList.add(map);
            }
            ProductSubAdapter productAdapter=new ProductSubAdapter(activity,hashMapArrayList);
            recyclerView.setAdapter(productAdapter);
            recyclerView.setFocusable(false);
        }catch (Exception e){
            Log.e("userProductSubListerror",e.toString());
        }

    }
}


