package com.convergentinfoware.gasagricultura.Adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.convergentinfoware.gasagricultura.Activity.ManageProductActivity;
import com.convergentinfoware.gasagricultura.Activity.ProductEditActivity;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.Helper.MySingleton;
import com.convergentinfoware.gasagricultura.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.IS_AVAILABLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.MethodClass.isNetworkConnected;

public class ManageProductAdapter extends RecyclerView.Adapter<ManageProductAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ManageProductAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.manage_product_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("Range")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.product_name_tv.setText(map.get(TITLE));
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(PRODCT_IMAGE)).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);

        //holder.qty_edt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(1)});
        //holder.price_edt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(1)});

        if (map.get(IS_AVAILABLE).equals("Y")) {
            holder.act_inact_btn.setTag("Y");
            holder.act_inact_btn.setText(activity.getResources().getString(R.string.inactivate));
            holder.act_inact_btn.setBackground(activity.getDrawable(R.drawable.gray_button_background));
        } else {
            holder.act_inact_btn.setTag("N");
            holder.act_inact_btn.setText(activity.getResources().getString(R.string.activate));
            holder.act_inact_btn.setBackground(activity.getDrawable(R.drawable.button_green_background));
            holder.product_name_tv.setTextColor(activity.getColor(R.color.gray_inactive));
            holder.disp_text.setTextColor(activity.getColor(R.color.gray_inactive));
            holder.price_text.setTextColor(activity.getColor(R.color.gray_inactive));
            holder.line_text.setTextColor(activity.getColor(R.color.gray_inactive));
            holder.product_image.setAlpha((float) 0.3);
            // holder.qty_unit.setTextColor(activity.getColor(R.color.gray_inactive));
            // holder.qty_edt.setTextColor(activity.getColor(R.color.gray_inactive));
            // holder.price_edt.setTextColor(activity.getColor(R.color.gray_inactive));
            // holder.price_unit.setTextColor(activity.getColor(R.color.gray_inactive));
             holder.price_tv.setTextColor(activity.getColor(R.color.gray_inactive));
             holder.qty_tv.setTextColor(activity.getColor(R.color.gray_inactive));

        }
        try {
            if (!map.get(PRODUCT_PRICE).equals("null") && !map.get(PRODUCT_PRICE).equals(null)) {
                JSONArray product_priceArray = new JSONArray(map.get(PRODUCT_PRICE));
                if (product_priceArray.length() != 0) {
                    for (int j = 0; j < product_priceArray.length(); j++) {
                        JSONObject object = product_priceArray.getJSONObject(j);
                        String id = object.getString("id");
                        String price = object.getString("price");
                        String qty = object.getString("qty");
                        String unit = object.getJSONObject("product_price_unit").getString("unit");
                       // holder.qty_edt.setText(qty.replace(".", ","));
                       // holder.qty_unit.setText(unit);
                       // holder.price_edt.setText(price.replace(".", ","));
                       // holder.price_unit.setText("€/" + unit);
                        holder.qty_tv.setText(qty.replace(".", ",")+unit);
                        holder.price_tv.setText(price.replace(".", ",")+"€/" + unit);
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ProductEditActivity.class);
                intent.putExtra("product_id", map.get(ID));
                activity.startActivity(intent);
            }
        });
       /* holder.check_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateQtyPrice(map.get(ID), holder.qty_edt.getText().toString().trim().replace(",", "."), holder.price_edt.getText().toString().trim().replace(",", "."));
            }
        });*/
        holder.act_inact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(holder.act_inact_btn.getTag()).equals("Y")) {
                    activeInactive(map.get(ID), "N");
                } else {
                    activeInactive(map.get(ID), "Y");
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView product_image, edit_img;
        private TextView product_name_tv;
        private Button act_inact_btn;
        private TextView disp_text, price_text, line_text;
       // private TextView qty_unit, price_unit, check_tv;
        //private EditText qty_edt, price_edt;
        private TextView qty_tv,price_tv;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_name_tv = itemView.findViewById(R.id.product_name_tv);
            edit_img = itemView.findViewById(R.id.edit_img);
            act_inact_btn = itemView.findViewById(R.id.act_inact_btn);
            disp_text = itemView.findViewById(R.id.disp_text);
            price_text = itemView.findViewById(R.id.price_text);
            line_text = itemView.findViewById(R.id.line_text);

            //qty_edt = itemView.findViewById(R.id.qty_edt);
            // price_edt = itemView.findViewById(R.id.price_edt);
            // check_tv = itemView.findViewById(R.id.check_tv);
            //  qty_unit = itemView.findViewById(R.id.qty_unit);
            //  price_unit = itemView.findViewById(R.id.price_unit);
              qty_tv = itemView.findViewById(R.id.qty_tv);
            price_tv = itemView.findViewById(R.id.price_tv);

        }
    }


    private void updateQtyPrice(String id, String qty, String price) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "update-product-qty";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", id);
        params.put("qty", qty);
        params.put("price", price);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("parames", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                ((ManageProductActivity) activity).get_list();
                if (resultResponse != null) {
                    Log.e("LOg", "jfgfji");
                }
               /* try {


                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("productError",e.toString());
                }*/


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }


    private void activeInactive(String id, String status) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "avail-product";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", id);
        params.put("status", status);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("parames", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("resp", response.toString());
                JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                ((ManageProductActivity) activity).get_list();
                if (resultResponse != null) {

                    new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE).setTitleText(activity.getResources().getString(R.string.product_status))
                            .setContentText(activity.getResources().getString(R.string.product_status_updated_successfully))
                            .setConfirmText(activity.getResources().getString(R.string.done)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    }).show();
                }
                /*try {


                } catch (JSONException e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("productError",e.toString());
                }*/


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("error2", error.getMessage() );

                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }


    class DecimalDigitsInputFilter implements InputFilter {
        private final int decimalDigits;
        /**
         * Constructor.
         *
         * @param decimalDigits maximum decimal digits
         */
        public DecimalDigitsInputFilter(int decimalDigits) {
            this.decimalDigits = decimalDigits;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            int dotPos = -1;
            int len = dest.length();
            for (int i = 0; i < len; i++) {
                char c = dest.charAt(i);
                if (c == '.' || c == ',') {
                    dotPos = i;
                    break;
                }
            }
            if (dotPos >= 0) {
                // protects against many dots
                if (source.equals(".") || source.equals(",")) {
                    return "";
                }
                // if the text is entered before the dot
                if (dend <= dotPos) {
                    return null;
                }
                if (len - dotPos > decimalDigits) {
                    return "";
                }
            }
            return null;
        }
    }
}


