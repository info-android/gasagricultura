package com.convergentinfoware.gasagricultura.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PEODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;

public class SellerOrderDetailsAdapter extends RecyclerView.Adapter<SellerOrderDetailsAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public SellerOrderDetailsAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.seller_order_details_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HashMap<String, String> map = map_list.get(position);
        holder.product_name_tv.setText(map.get(TITLE));
        holder.qty_tv.setText(map.get(QTY)+" "+map.get(PEODUCT_UNIT));
        holder.product_price_tv.setText(map.get(PRODUCT_PRICE).replace(".",",")+" €");
        holder.user_name_tv.setText(map.get(USER_NAME));

        Picasso.get().load(PRODUCT_IMAGE_URL+map.get(PRODCT_IMAGE)).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);
        Picasso.get().load(PROFILE_IMAGE_URL+map.get(USER_IMAGE)).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.user_image);

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView product_image;
        private TextView product_name_tv,qty_tv,product_price_tv,user_name_tv;
        private CircleImageView user_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            product_image=itemView.findViewById(R.id.product_image);
            product_name_tv=itemView.findViewById(R.id.product_name_tv);
            qty_tv=itemView.findViewById(R.id.qty_tv);
            product_price_tv=itemView.findViewById(R.id.product_price_tv);
            user_image=itemView.findViewById(R.id.user_image);
            user_name_tv=itemView.findViewById(R.id.user_name_tv);
        }
    }

}


