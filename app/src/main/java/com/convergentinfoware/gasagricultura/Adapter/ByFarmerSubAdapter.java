package com.convergentinfoware.gasagricultura.Adapter;

import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.ByOrderDetailsActivity;
import com.convergentinfoware.gasagricultura.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NO_OF_ITEMS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_NO;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TOTAL_WEIGHT_TXT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;

public class ByFarmerSubAdapter extends RecyclerView.Adapter<ByFarmerSubAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ByFarmerSubAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.by_farmer_sub_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        if (position%2!=0){
            holder.item_layout.setBackgroundColor(activity.getColor(R.color.light_gray_background));
        }

        holder.order_no_tv.setText(activity.getResources().getString(R.string.order_no_with_colon)+map.get(ORDER_NO));
        holder.user_name_tv.setText(map.get(USER_NAME));
        holder.total_product_tv.setText(map.get(NO_OF_ITEMS));
        holder.order_qty_tv.setText(map.get(TOTAL_WEIGHT_TXT));
        holder.product_price_tv.setText(map.get(ORDER_TOTAL).replace(".",",")+"€");
        try{
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
            Date date= dateFormat.parse(map.get(ORDER_DATE));
            holder.order_date_tv.setText(DateFormat.format("dd MMM, yyyy",date));
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.viewLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, ByOrderDetailsActivity.class);
                intent.putExtra("order_id",map.get(ID));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout item_layout,viewLay;
        private TextView order_no_tv,user_name_tv,order_date_tv,total_product_tv,order_qty_tv,product_price_tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout=itemView.findViewById(R.id.item_layout);
            viewLay=itemView.findViewById(R.id.viewLay);
            order_no_tv=itemView.findViewById(R.id.order_no_tv);
            user_name_tv=itemView.findViewById(R.id.user_name_tv);
            order_date_tv=itemView.findViewById(R.id.order_date_tv);
            total_product_tv=itemView.findViewById(R.id.total_product_tv);
            order_qty_tv=itemView.findViewById(R.id.order_qty_tv);
            product_price_tv=itemView.findViewById(R.id.product_price_tv);
        }
    }

}


