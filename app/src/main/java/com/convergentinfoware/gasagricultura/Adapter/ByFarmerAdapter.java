package com.convergentinfoware.gasagricultura.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.DESCRIPTION;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NO_OF_ITEMS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_NO;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.SELLER_ORDER;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TOTAL_WEIGHT_TXT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_PRODUCTS;

public class ByFarmerAdapter extends RecyclerView.Adapter<ByFarmerAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ByFarmerAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.by_farmer_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HashMap<String, String> map = map_list.get(position);
        holder.name_tv.setText(map.get(NAME));
        String profile_image = PROFILE_IMAGE_URL + map.get(PROFILE_IMAGE);
        Picasso.get().load(profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.user_image);
        get_list(holder.sub_recy_view,map.get(NAME),map.get(PROFILE_IMAGE),map.get(SELLER_ORDER));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView sub_recy_view;
        private CircleImageView user_image;
        private TextView name_tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sub_recy_view=itemView.findViewById(R.id.sub_recy_view);
            user_image=itemView.findViewById(R.id.user_image);
            name_tv=itemView.findViewById(R.id.name_tv);
        }
    }




    private void get_list(RecyclerView recyclerView,String user_name,String user_img, String seller_order) {
        try {
            JSONArray jsonArray=new JSONArray(seller_order);
            ArrayList<HashMap<String,String>> hashMapArrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(USER_NAME, user_name);
                map.put(USER_IMAGE, user_img);
                map.put(ID, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("id"));
                map.put(ORDER_NO, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("order_no"));
                map.put(TOTAL_WEIGHT_TXT, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("total_weight_txt"));
                map.put(ORDER_DATE, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("order_date"));
                map.put(NO_OF_ITEMS, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("no_of_items"));
                map.put(ORDER_TOTAL, jsonArray.getJSONObject(i).getJSONObject("seller_order").getString("order_total"));
                //map.put(ORDER_TOTAL, jsonArray.getJSONObject(i).getString("total"));
                hashMapArrayList.add(map);
            }
            ByFarmerSubAdapter byFarmerSubAdapter=new ByFarmerSubAdapter(activity,hashMapArrayList);
            recyclerView.setAdapter(byFarmerSubAdapter);
            recyclerView.setFocusable(false);
        }catch (Exception e){
            Log.e("userProductSubListerror",e.toString());
        }

    }
}


