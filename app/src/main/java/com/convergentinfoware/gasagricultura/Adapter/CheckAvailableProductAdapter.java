package com.convergentinfoware.gasagricultura.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.MyShoppingBagActivity;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.AVA_QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;

public class CheckAvailableProductAdapter extends RecyclerView.Adapter<CheckAvailableProductAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String,String>> map_list;

    public CheckAvailableProductAdapter(Activity activity, ArrayList<HashMap<String,String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.check_ava_pro_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final HashMap<String,String> map = map_list.get(position);
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(PRODUCT_IMAGE)).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);
        holder.product_title_tv.setText(map.get(TITLE));
        holder.product_qty_tv.setText(map.get(AVA_QTY) +" "+map.get(PRODUCT_UNIT));
        holder.product_price_tv.setText(map.get(PRODUCT_PRICE) + " €");
        Picasso.get().load(PROFILE_IMAGE_URL + map.get(PROFILE_IMAGE)).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.seller_image);
        holder.seller_name.setText(map.get(USER_NAME));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView seller_image;
        private TextView product_title_tv, product_qty_tv, product_price_tv, seller_name;
        private ImageView product_image, delete_img, qty_minus_img, qty_plus_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            seller_image = itemView.findViewById(R.id.seller_image);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            product_qty_tv = itemView.findViewById(R.id.product_qty_tv);
            product_price_tv = itemView.findViewById(R.id.product_price_tv);
            seller_name = itemView.findViewById(R.id.seller_name);
            product_image = itemView.findViewById(R.id.product_image);
            delete_img = itemView.findViewById(R.id.delete_img);
            qty_minus_img = itemView.findViewById(R.id.qty_minus_img);
            qty_plus_img = itemView.findViewById(R.id.qty_plus_img);
        }
    }

}


