package com.convergentinfoware.gasagricultura.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.FarmerProductListActivity;
import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NAME;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;

public class FarmerAdapter extends RecyclerView.Adapter<FarmerAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public FarmerAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.farmer_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.name_tv.setText(map.get(NAME));
        String profile_image = PROFILE_IMAGE_URL + map.get(PROFILE_IMAGE);
        Picasso.get().load(profile_image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.image);

        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, FarmerProductListActivity.class);
                intent.putExtra("user_id",map.get(ID));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout item_layout;
        private TextView name_tv;
        private CircleImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout=itemView.findViewById(R.id.item_layout);
            name_tv=itemView.findViewById(R.id.name_tv);
            image=itemView.findViewById(R.id.image);
        }
    }
}


