package com.convergentinfoware.gasagricultura.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.MyShoppingBagActivity;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;

public class MyShoppingBagAdapter extends RecyclerView.Adapter<MyShoppingBagAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<MethodClass.CartClass> map_list;

    public MyShoppingBagAdapter(Activity activity, ArrayList<MethodClass.CartClass> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_shopping_bag_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final MethodClass.CartClass cartClass = map_list.get(position);
        Picasso.get().load(PRODUCT_IMAGE_URL + cartClass.product_img).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);
        holder.product_title_tv.setText(cartClass.product_title);
        holder.product_qty_tv.setText(String.valueOf((int)cartClass.product_qty) + " " + cartClass.product_unit);
        float product_pri = Float.valueOf(cartClass.product_qty) * Float.valueOf(cartClass.product_price);
        holder.product_price_tv.setText(String.valueOf(product_pri).replace(".",",") + " €");
        Log.e("product_user_img",PROFILE_IMAGE_URL + cartClass.product_user_img);
        Picasso.get().load(PROFILE_IMAGE_URL + cartClass.product_user_img).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(holder.seller_image);
        holder.seller_name.setText(cartClass.product_user_name);

        holder.qty_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((cartClass.product_qty - 1) > 0) {
                    map_list.set(position, new MethodClass.CartClass(cartClass.product_user_id, cartClass.product_user_name, cartClass.product_user_img, cartClass.product_id, cartClass.product_title, cartClass.product_img, cartClass.product_qty - 1, Float.valueOf(cartClass.product_total_qty), cartClass.product_price,cartClass.product_unit_id, cartClass.product_unit));
                } else {
                    map_list.remove(position);
                }
                ((MyShoppingBagActivity) activity).finalCartCalculation();
                ((MyShoppingBagActivity) activity).get_list();
            }
        });
        holder.qty_plus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Float.valueOf(cartClass.product_total_qty) > cartClass.product_qty) {
                    map_list.set(position, new MethodClass.CartClass(cartClass.product_user_id, cartClass.product_user_name, cartClass.product_user_img, cartClass.product_id, cartClass.product_title, cartClass.product_img, cartClass.product_qty + 1, Float.valueOf(cartClass.product_total_qty), cartClass.product_price,cartClass.product_unit_id, cartClass.product_unit));
                    ((MyShoppingBagActivity) activity).finalCartCalculation();
                    ((MyShoppingBagActivity) activity).get_list();
                } else {
                    Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), " Product disp not available", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                map_list.remove(position);
                ((MyShoppingBagActivity) activity).finalCartCalculation();
                ((MyShoppingBagActivity) activity).get_list();
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView seller_image;
        private TextView product_title_tv, product_qty_tv, product_price_tv, seller_name;
        private ImageView product_image, delete_img, qty_minus_img, qty_plus_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            seller_image = itemView.findViewById(R.id.seller_image);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            product_qty_tv = itemView.findViewById(R.id.product_qty_tv);
            product_price_tv = itemView.findViewById(R.id.product_price_tv);
            seller_name = itemView.findViewById(R.id.seller_name);
            product_image = itemView.findViewById(R.id.product_image);
            delete_img = itemView.findViewById(R.id.delete_img);
            qty_minus_img = itemView.findViewById(R.id.qty_minus_img);
            qty_plus_img = itemView.findViewById(R.id.qty_plus_img);
        }
    }

}


