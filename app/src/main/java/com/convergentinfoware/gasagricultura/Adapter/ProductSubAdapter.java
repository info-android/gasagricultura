package com.convergentinfoware.gasagricultura.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.convergentinfoware.gasagricultura.Activity.ProductActivity;
import com.convergentinfoware.gasagricultura.Activity.ProductDetailsActivity;
import com.convergentinfoware.gasagricultura.Helper.ConstantClass;
import com.convergentinfoware.gasagricultura.Helper.MethodClass;
import com.convergentinfoware.gasagricultura.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_PRICE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;

public class ProductSubAdapter extends RecyclerView.Adapter<ProductSubAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ProductSubAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.product_sub_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String, String> map = map_list.get(position);
        if (position % 2 != 0) {
            holder.item_layout.setBackgroundColor(activity.getColor(R.color.light_gray_background));
        }

        holder.item_layoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("product_id", map.get(ID));
                activity.startActivity(intent);
            }
        });
        holder.product_title_tv.setText(map.get(TITLE));
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(PRODCT_IMAGE)).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);
        try {
            if (!map.get(PRODUCT_PRICE).equals("null") && !map.get(PRODUCT_PRICE).equals(null)) {
                JSONArray product_priceArray = new JSONArray(map.get(PRODUCT_PRICE));
                Log.e("PRODUCT_PRICE", PRODUCT_PRICE);
                if (product_priceArray.length() != 0) {
                    Log.e("PRODUCT_PRICELL", String.valueOf(product_priceArray.length()));
                    for (int j = 0; j < product_priceArray.length(); j++) {
                        JSONObject object = product_priceArray.getJSONObject(j);
                        Log.e("PRODUCT_PRICE_OB", object.toString());
                        String id = object.getString("id");
                        String price = object.getString("price");
                        String qty = object.getString("qty");
                        String unit_id = object.getJSONObject("product_price_unit").getString("id");
                        String unit = object.getJSONObject("product_price_unit").getString("unit");
                        holder.product_price_tv.setTag(price);
                        holder.product_price_tv.setText(price.replace(".",",") + " €/" + unit);
                        holder.product_price_tv.setId(Integer.parseInt(unit_id));
                        holder.product_qty_tv.setText(qty + " " + unit);
                        holder.product_qty_tv.setTag(qty);
                        holder.product_title_tv.setTag(unit);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.add_to_cart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userloged_id= PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");
                if (!map.get(USER_ID).equals(userloged_id)) {

                    float tempQty = 0;
                    for (int i = 0; i < ConstantClass.CART_ARRAY.size(); i++) {
                        MethodClass.CartClass cartClass = ConstantClass.CART_ARRAY.get(i);
                        if (map.get(ID).equals(cartClass.product_id)) {
                            tempQty = cartClass.product_qty;
                            if (Float.valueOf((String) holder.product_qty_tv.getTag()) > tempQty) {
                                ConstantClass.CART_ARRAY.remove(i);
                            }
                        }
                    }
                    if (Float.valueOf((String) holder.product_qty_tv.getTag()) > tempQty) {
                        ConstantClass.CART_ARRAY.add(new MethodClass.CartClass(map.get(USER_ID), map.get(USER_NAME), map.get(USER_IMAGE), map.get(ID), map.get(TITLE), map.get(PRODCT_IMAGE), tempQty + 1, Float.valueOf((String) holder.product_qty_tv.getTag()), String.valueOf(holder.product_price_tv.getTag()), String.valueOf(holder.product_price_tv.getId()), String.valueOf(holder.product_title_tv.getTag())));

                        Snackbar snackbar = Snackbar.make(holder.item_layout, "", Snackbar.LENGTH_SHORT);
                        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbarLayout.setBackground(activity.getDrawable(R.color.transpaent));
                        View view1 = LayoutInflater.from(activity).inflate(R.layout.snackbar_view, null);
                        TextView text = view1.findViewById(R.id.text);
                        text.setText(activity.getResources().getString(R.string.just_added));
                        snackbarLayout.addView(view1);
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(holder.item_layout, "", Snackbar.LENGTH_SHORT);
                        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbarLayout.setBackground(activity.getDrawable(R.color.transpaent));
                        View view1 = LayoutInflater.from(activity).inflate(R.layout.snackbar_view, null);
                        TextView text = view1.findViewById(R.id.text);
                        text.setText(activity.getResources().getString(R.string.product_disp_not_available));
                        snackbarLayout.addView(view1);
                        snackbar.show();
                    }
                    MethodClass.cartCalculation(activity);
                }else {
                    Snackbar snackbar = Snackbar.make(holder.item_layout, "", Snackbar.LENGTH_SHORT);
                    Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                    snackbarLayout.setBackground(activity.getDrawable(R.color.transpaent));
                    View view1 = LayoutInflater.from(activity).inflate(R.layout.snackbar_view, null);
                    TextView text = view1.findViewById(R.id.text);
                    text.setText(activity.getResources().getString(R.string.you_cant_add_own_product));
                    snackbarLayout.addView(view1);
                    snackbar.show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout  item_layoutClick, add_to_cart_btn;
        private TextView product_title_tv, product_price_tv, product_qty_tv;
        private ImageView product_image;
        private CoordinatorLayout item_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout = itemView.findViewById(R.id.item_layout);
            item_layoutClick = itemView.findViewById(R.id.item_layoutClick);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            product_price_tv = itemView.findViewById(R.id.product_price_tv);
            product_qty_tv = itemView.findViewById(R.id.product_qty_tv);
            product_image = itemView.findViewById(R.id.product_image);
            add_to_cart_btn = itemView.findViewById(R.id.add_to_cart_btn);
        }
    }

}


