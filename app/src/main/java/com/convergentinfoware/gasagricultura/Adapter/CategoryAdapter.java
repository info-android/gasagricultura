package com.convergentinfoware.gasagricultura.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.FilterActivity;
import com.convergentinfoware.gasagricultura.Activity.ProductActivity;
import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Activity.FilterActivity.CAT_CHECK_LIST;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.CATEGORY_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public CategoryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.category_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.cat_name_tv.setText(map.get(TITLE));
        //holder.item_layout.setId(Integer.parseInt(map.get(ID)));
        String catImg=CATEGORY_IMAGE_URL + map.get(IMAGE);
        Log.e("catImg",catImg);
        if ( map.get(IMAGE).equals("all")){
            holder.all_text.setVisibility(View.VISIBLE);
           holder.background_black.setVisibility(View.VISIBLE);
           holder.cat_img.setVisibility(View.GONE);
        }else {
            holder.all_text.setVisibility(View.GONE);
            holder.background_black.setVisibility(View.GONE);
            Picasso.get().load(catImg).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.cat_img);
        }
        /*final boolean[] check = {false};*/
        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, ProductActivity.class);
                intent.putExtra("key","");
                intent.putExtra("cat_id",map.get(ID));
                activity.startActivity(intent);
                /*if (!check[0]){
                    check[0] =true;
                    holder.check_img.setVisibility(View.VISIBLE);
                    holder.background_green.setVisibility(View.VISIBLE);
                    holder.relative_lay.setBackground(activity.getDrawable(R.drawable.filter_check));
                    for (int i = 0; i < CAT_CHECK_LIST.size(); i++) {
                        if (CAT_CHECK_LIST.get(i)==holder.item_layout.getId()){
                            CAT_CHECK_LIST.remove(i);
                        }
                    }
                    CAT_CHECK_LIST.add(holder.item_layout.getId());
                } else {
                    check[0] =false;
                    holder.check_img.setVisibility(View.GONE);
                    holder.background_green.setVisibility(View.GONE);
                    holder.relative_lay.setBackground(null);
                    for (int i = 0; i < CAT_CHECK_LIST.size(); i++) {
                        if (CAT_CHECK_LIST.get(i)==holder.item_layout.getId()){
                            CAT_CHECK_LIST.remove(i);
                        }
                    }
                }*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout item_layout;
        private TextView cat_name_tv,all_text;
        private CircleImageView cat_img;
        private ImageView check_img,background_black;
        private RelativeLayout relative_lay;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout = itemView.findViewById(R.id.item_layout);
            cat_name_tv = itemView.findViewById(R.id.cat_name_tv);
            all_text = itemView.findViewById(R.id.all_text);
            cat_img = itemView.findViewById(R.id.cat_img);
           // check_img = itemView.findViewById(R.id.check_img);
            background_black = itemView.findViewById(R.id.background_black);
            relative_lay = itemView.findViewById(R.id.relative_lay);
        }
    }
}


