package com.convergentinfoware.gasagricultura.Adapter;

import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.SellerOrderDetailsActivity;
import com.convergentinfoware.gasagricultura.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ARCHIVEED;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.FROMDATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ID;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.NO_OF_ITEMS;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_DATE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.ORDER_TOTAL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_IMAGE_URL;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PRODUCT_UNIT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.QTY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TITLE;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.TOTAL_WEIGHT_TXT;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.USER_NAME;

public class SellerOrderProductListAdapter extends RecyclerView.Adapter<SellerOrderProductListAdapter.ViewHolder> {
    AppCompatActivity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public SellerOrderProductListAdapter(AppCompatActivity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.seller_order_prodct_list_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        if (position%2!=0){
            holder.item_layout.setBackgroundColor(activity.getColor(R.color.light_gray_background));
        }
        holder.order_title_tv.setText(activity.getResources().getString(R.string.product_with_colon)+map.get(TITLE));
        Picasso.get().load(PRODUCT_IMAGE_URL+map.get(PRODUCT_IMAGE)).placeholder(R.drawable.ic_load_img).error(R.drawable.ic_load_img).into(holder.product_image);
        holder.order_qty_tv.setText(map.get(NO_OF_ITEMS)+" "+map.get(PRODUCT_UNIT));
        holder.product_price_tv.setText(map.get(ORDER_TOTAL).replace(".",",")+"€");
        try{
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
            Date date= dateFormat.parse(map.get(ORDER_DATE));
            holder.order_date_tv.setText(DateFormat.format("dd MMM, yyyy",date));
            /*SimpleDateFormat fromDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate=fromDateFormat.parse(map.get(FROMDATE));
            if (date.before(fromDate)){
                holder.item_layout.setAlpha((float) 0.3);
            }*/
        }catch (Exception e){
            e.printStackTrace();
        }
        holder.viewLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity, SellerOrderDetailsActivity.class);
                intent.putExtra("order_id",map.get(ID));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout item_layout,viewLay;
        private TextView order_title_tv,order_date_tv,order_qty_tv,product_price_tv;
        private ImageView product_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout=itemView.findViewById(R.id.item_layout);
            viewLay=itemView.findViewById(R.id.viewLay);
            order_title_tv =itemView.findViewById(R.id.order_title_tv);
            order_date_tv=itemView.findViewById(R.id.order_date_tv);
            order_qty_tv=itemView.findViewById(R.id.order_qty_tv);
            product_price_tv=itemView.findViewById(R.id.product_price_tv);
            product_image=itemView.findViewById(R.id.product_image);
        }
    }

}


