package com.convergentinfoware.gasagricultura.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.convergentinfoware.gasagricultura.Activity.ChooseUserActivity;
import com.convergentinfoware.gasagricultura.Activity.CloseOrderActivity;
import com.convergentinfoware.gasagricultura.Activity.LoginActivity;
import com.convergentinfoware.gasagricultura.Activity.ManageOpenCloseActivity;
import com.convergentinfoware.gasagricultura.Activity.ManageProductActivity;
import com.convergentinfoware.gasagricultura.Activity.MyShoppingBagActivity;
import com.convergentinfoware.gasagricultura.Activity.PreviousOrderActivity;
import com.convergentinfoware.gasagricultura.Activity.ProductAddActivity;
import com.convergentinfoware.gasagricultura.Activity.ProfileEditActivity;
import com.convergentinfoware.gasagricultura.Activity.SellerOrderListActivity;
import com.convergentinfoware.gasagricultura.R;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.CART_ARRAY;
import static com.convergentinfoware.gasagricultura.Helper.ConstantClass.PROFILE_IMAGE_URL;

public class MethodClass {

    public static void hide_keyboard(Context context) {
        ((AppCompatActivity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    static Dialog mDialog;

    public static void showProgressDialog(Activity activity) {
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    public static void hideProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void setMenu(final Activity activity) {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        LinearLayout my_account, shop, my_order, my_pre_order, shopping_cart, seller_menu_layout, manage_product, addproduct, my_sells, manage_open_close, logout, all_seller_lay;
        final TextView my_account_tv, shop_tv, my_order_tv, my_pre_order_tv, shopping_cart_tv, manage_product_tv, addproduct_tv, my_sells_tv, manage_open_close_tv, logout_tv;

        seller_menu_layout = headerView.findViewById(R.id.seller_menu_layout);
        all_seller_lay = headerView.findViewById(R.id.all_seller_lay);
        manage_open_close = headerView.findViewById(R.id.manage_open_close);

        String is_seller = PreferenceManager.getDefaultSharedPreferences(activity).getString("is_seller", "");
        String user_type = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_type", "");
        if (user_type.equals("A")) {
            all_seller_lay.setVisibility(View.GONE);
            manage_open_close.setVisibility(View.VISIBLE);
        } else {
            all_seller_lay.setVisibility(View.VISIBLE);
            manage_open_close.setVisibility(View.GONE);
        }
        if (is_seller.equals("Y")) {
            seller_menu_layout.setVisibility(View.VISIBLE);
        } else {
            seller_menu_layout.setVisibility(View.GONE);
        }


        String user_name = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_name", "");
        String email = PreferenceManager.getDefaultSharedPreferences(activity).getString("email", "");
        String image = PROFILE_IMAGE_URL + PreferenceManager.getDefaultSharedPreferences(activity).getString("profile_pic", "");

        TextView nav_name_tv = headerView.findViewById(R.id.nav_name_tv);
        TextView nav_email_tv = headerView.findViewById(R.id.nav_email_tv);
        CircleImageView nav_pro_img = headerView.findViewById(R.id.nav_pro_img);

        nav_name_tv.setText(""+activity.getResources().getString(R.string.hi)+", " + user_name);
        nav_email_tv.setText(email);
        Picasso.get().load(image).placeholder(R.drawable.farmers_icon).error(R.drawable.farmers_icon).into(nav_pro_img);


        my_account = headerView.findViewById(R.id.my_account);
        shop = headerView.findViewById(R.id.shop);
        my_order = headerView.findViewById(R.id.my_order);
        my_pre_order = headerView.findViewById(R.id.my_pre_order);
        shopping_cart = headerView.findViewById(R.id.shopping_cart);
        manage_product = headerView.findViewById(R.id.manage_product);
        addproduct = headerView.findViewById(R.id.addproduct);
        my_sells = headerView.findViewById(R.id.my_sells);
        logout = headerView.findViewById(R.id.logout);

        my_account_tv = headerView.findViewById(R.id.my_account_tv);
        shop_tv = headerView.findViewById(R.id.shop_tv);
        my_order_tv = headerView.findViewById(R.id.my_order_tv);
        my_pre_order_tv = headerView.findViewById(R.id.my_pre_order_tv);
        shopping_cart_tv = headerView.findViewById(R.id.shopping_cart_tv);
        manage_product_tv = headerView.findViewById(R.id.manage_product_tv);
        addproduct_tv = headerView.findViewById(R.id.addproduct_tv);
        my_sells_tv = headerView.findViewById(R.id.my_sells_tv);
        manage_open_close_tv = headerView.findViewById(R.id.manage_open_close_tv);
        logout_tv = headerView.findViewById(R.id.logout_tv);


        my_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                my_account_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, ProfileEditActivity.class);
                activity.startActivity(intent);
            }
        });
        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                shop_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, ChooseUserActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });

        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                my_order_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, CloseOrderActivity.class);
                activity.startActivity(intent);
            }
        });
        my_pre_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                my_pre_order_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, PreviousOrderActivity.class);
                activity.startActivity(intent);
            }
        });

        shopping_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                shopping_cart_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, MyShoppingBagActivity.class);
                activity.startActivity(intent);
            }
        });


        manage_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                manage_product_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, ManageProductActivity.class);
                activity.startActivity(intent);
            }
        });

        addproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                addproduct_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, ProductAddActivity.class);
                activity.startActivity(intent);
            }
        });

        my_sells.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                my_sells_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, SellerOrderListActivity.class);
                activity.startActivity(intent);
            }
        });
        manage_open_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                manage_open_close_tv.setTextColor(activity.getColor(R.color.green));
                Intent intent = new Intent(activity, ManageOpenCloseActivity.class);
                activity.startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDefaultColor(activity);
                logout_tv.setTextColor(activity.getColor(R.color.green));
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE).setTitleText(activity.getResources().getString(R.string.log_ouy))
                        .setContentText(activity.getResources().getString(R.string.do_you_want_to_log_out)).setConfirmText(activity.getResources().getString(R.string.okay)).setCancelText(activity.getResources().getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("is_logged_in", false).commit();
                        CART_ARRAY.clear();
                        cartCalculation(activity);
                        sDialog.dismissWithAnimation();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }).show();

            }
        });

    }

    public static void changeDefaultColor(Activity activity) {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        final TextView my_account_tv, my_order_tv, my_pre_order_tv, shopping_cart_tv, manage_product_tv, addproduct_tv, my_sells_tv, logout_tv, shop_tv, manage_open_close_tv;

        my_account_tv = headerView.findViewById(R.id.my_account_tv);
        my_order_tv = headerView.findViewById(R.id.my_order_tv);
        my_pre_order_tv = headerView.findViewById(R.id.my_pre_order_tv);
        shopping_cart_tv = headerView.findViewById(R.id.shopping_cart_tv);
        manage_product_tv = headerView.findViewById(R.id.manage_product_tv);
        addproduct_tv = headerView.findViewById(R.id.addproduct_tv);
        my_sells_tv = headerView.findViewById(R.id.my_sells_tv);
        logout_tv = headerView.findViewById(R.id.logout_tv);
        shop_tv = headerView.findViewById(R.id.shop_tv);
        manage_open_close_tv = headerView.findViewById(R.id.manage_open_close_tv);

        my_account_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        my_order_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        my_pre_order_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        shopping_cart_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        manage_product_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        addproduct_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        my_sells_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        logout_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        shop_tv.setTextColor(activity.getColor(R.color.nav_text_black));
        manage_open_close_tv.setTextColor(activity.getColor(R.color.nav_text_black));
    }

    public static JSONObject Json_rpc_format(HashMap<String, String> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        return new JSONObject(main_param);
    }

    public static JSONObject Json_rpc_format_obj(HashMap<String, Object> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        return new JSONObject(main_param);
    }

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static JSONObject get_result_from_webservice(Activity activity, JSONObject response) {
        JSONObject result = null;
        if (response.has("error")) {
            try {
                String error = response.getString("error");
                JSONObject jsonObject = new JSONObject(error);
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText(jsonObject.getString("message")).setContentText(jsonObject.getString("meaning")).setConfirmText(activity.getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.has("result")) {
            try {
                result = response.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("methodeJSONException", e.toString());
            }

        } else {
            Log.e("methodeElse", "methode Else");
            error_alert(activity);
        }
        return result;
    }

    public static void error_alert(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText(activity.getResources().getString(R.string.oops))
                        .setContentText(activity.getResources().getString(R.string.something_went_wrong)).setConfirmText(activity.getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void network_error_alert(final Activity activity) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(activity.getResources().getString(R.string.network_error))
                .setContentText(activity.getResources().getString(R.string.please_check_your_internet_connection))
                .setConfirmText(activity.getResources().getString(R.string.settings)).setCancelText(activity.getResources().getString(R.string.okay)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        }).show();
    }


    public static String getRightAngleImage(String photoPath) {

        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degree = 0;

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                default:
                    degree = 90;
            }

            return rotateImage(degree, photoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoPath;
    }

    public static String rotateImage(int degree, String imagePath) {

        if (degree <= 0) {
            return imagePath;
        }
        try {
            Bitmap b = BitmapFactory.decodeFile(imagePath);

            Matrix matrix = new Matrix();
            if (b.getWidth() > b.getHeight()) {
                matrix.setRotate(degree);
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
            }

            FileOutputStream fOut = new FileOutputStream(imagePath);
            String imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
            String imageType = imageName.substring(imageName.lastIndexOf(".") + 1);

            FileOutputStream out = new FileOutputStream(imagePath);
            if (imageType.equalsIgnoreCase("png")) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else if (imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            fOut.flush();
            fOut.close();

            b.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }


    public static class StringWithTag {
        public String string;
        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public static class CartClass {
        public String product_user_id;
        public String product_user_name;
        public String product_user_img;
        public String product_id;
        public String product_title;
        public String product_img;
        public float product_qty;
        public float product_total_qty;
        public String product_price;
        public String product_unit_id;
        public String product_unit;

        public CartClass(String product_user_id, String product_user_name, String product_user_img, String product_id, String product_title, String product_img, float product_qty, float product_total_qty, String product_price, String product_unit_id, String product_unit) {
            this.product_user_id = product_user_id;
            this.product_user_name = product_user_name;
            this.product_user_img = product_user_img;
            this.product_id = product_id;
            this.product_title = product_title;
            this.product_img = product_img;
            this.product_qty = product_qty;
            this.product_total_qty = product_total_qty;
            this.product_price = product_price;
            this.product_unit_id = product_unit_id;
            this.product_unit = product_unit;
        }
    }


    public static void cartCalculation(final Activity activity) {
        TextView cart_tv = activity.findViewById(R.id.cart_tv);
        RelativeLayout cart_count_lay = activity.findViewById(R.id.cart_count_lay);
        String user_type = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_type", "");
        if (user_type.equals("A")) {
            cart_count_lay.setVisibility(View.GONE);
        } else {
            cart_count_lay.setVisibility(View.VISIBLE);
        }
        float totalCartCount = 0;
        for (int i = 0; i < ConstantClass.CART_ARRAY.size(); i++) {
            MethodClass.CartClass cartClass = ConstantClass.CART_ARRAY.get(i);
            totalCartCount = totalCartCount + cartClass.product_qty;
            Log.e("totalCartCount", String.valueOf(totalCartCount));
        }
        if (totalCartCount > 0) {
            cart_tv.setText(String.valueOf(CART_ARRAY.size()));
            cart_tv.setVisibility(View.VISIBLE);
        } else {
            cart_tv.setVisibility(View.GONE);
        }
        cart_count_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyShoppingBagActivity.class);
                activity.startActivity(intent);
            }
        });

        MyDbClass myDbClass = new MyDbClass(activity);
        SQLiteDatabase sqLiteDatabase = myDbClass.getWritableDatabase();
        if (ConstantClass.CART_ARRAY.size() > 0) {
            long deletedRows = sqLiteDatabase.delete(MyDbClass.DATA_TABLE, null, null);
            Gson gson = new Gson();
            String arrayAsString = gson.toJson(CART_ARRAY);
            ContentValues contentValues = new ContentValues();
            contentValues.put(MyDbClass.DATA, arrayAsString);
            long insertRows = sqLiteDatabase.insert(MyDbClass.DATA_TABLE, null, contentValues);
        } else {
            long deletedRows = sqLiteDatabase.delete(MyDbClass.DATA_TABLE, null, null);
        }

         /* if (ConstantClass.CART_ARRAY.size()>0) {
            Gson gson = new Gson();
            String arrayAsString = gson.toJson(CART_ARRAY);
            PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("cartArray", arrayAsString).commit();
            Log.e("arrayAsString", arrayAsString);
        }else {
            PreferenceManager.getDefaultSharedPreferences(activity).edit().remove("cartArray").commit();
        }*/

    }


    public static void set_locale(Activity activity) {
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale("it".toLowerCase())); // API 17+ only.
        }else {
            conf.locale = new Locale("it");
        }
        res.updateConfiguration(conf, dm);
    }

    public static String check_locale_lang(Activity activity) {
        String languageToLoad = PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG", "de");
        return languageToLoad;
    }

}
