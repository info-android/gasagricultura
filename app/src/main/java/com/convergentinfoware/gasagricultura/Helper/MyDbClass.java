package com.convergentinfoware.gasagricultura.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDbClass extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Aglicultura.db";
    public static final String DATA_TABLE = "data_table";
    public static final String DATA = "data";
    public static final String CREATE_TABLE = "CREATE TABLE " + DATA_TABLE + " (" + DATA + " TEXT)";

    public MyDbClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
        onCreate(sqLiteDatabase);
    }
}
