package com.convergentinfoware.gasagricultura.Helper;

import java.util.ArrayList;

public class ConstantClass {
    public static final String BASE_URL = "http://gasadmin.mercatoagricultura.it/preview/";
    public static final String PROFILE_IMAGE_URL = BASE_URL+"storage/app/public/images/user_image/";
    public static final String PRODUCT_IMAGE_URL = BASE_URL+"storage/app/public/images/product_images/";
    public static final String CATEGORY_IMAGE_URL = BASE_URL+"storage/app/public/images/category_images/";
    //http://gasadmin.mercatoagricultura.it/preview/storage/app/public/images/category_images/1581427276_56293categories7.png


    public static final int TAKE_PHOTO_CODE = 456;
    public static final int PICK_FROM_GALLERY = 654;
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String PROFILE_IMAGE = "PROFILE_IMAGE";
    public static final String USER_PRODUCTS = "USER_PRODUCTS";
    public static final String TITLE = "TITLE";
    public static final String IMAGE = "IMAGE";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String PRODCT_IMAGE = "PRODCT_IMAGE";
    public static final String PRODUCT_PRICE = "PRODUCT_PRICE";
    public static final String QTY = "QTY";
    public static final String IS_AVAILABLE = "IS_AVAILABLE";
    public static final String PEODUCT_UNIT = "PEODUCT_UNIT";
    public static final String PRODUCT_IMAGE = "PRODUCT_IMAGE";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String PRODUCT_UNIT = "PRODUCT_UNIT";
    public static final String USER_IMAGE = "USER_IMAGE";
    public static final String FROMDATE = "FROMDATE";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String AVA_QTY = "AVA_QTY";
    public static final String ORDER_NO = "ORDER_NO";
    public static final String ORDER_DATE = "ORDER_DATE";
    public static final String ORDER_TOTAL = "ORDER_TOTAL";
    public static final String TOTAL_WEIGHT_TXT = "TOTAL_WEIGHT_TXT";
    public static final String ARCHIVEED = "ARCHIVEED";
    public static final String STATUS = "STATUS";
    public static final String NO_OF_ITEMS = "NO_OF_ITEMS";
    public static final String SELLER_ORDER = "SELLER_ORDER";

    public static ArrayList<MethodClass.CartClass> CART_ARRAY=new ArrayList<MethodClass.CartClass>();


}
